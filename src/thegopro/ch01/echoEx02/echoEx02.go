// Exercise 2: Modify the echo program to print the index and value of each of its arguments, one per line.

// This echo1 program prints its command-line arguments on one line.

package main

import (
	"fmt"
	"os"
)

func main() {
	var s, space string // declare veriable s, and sep of type string
	for i := 1; i < len(os.Args); i++ {
		s += space + os.Args[i] // concatenation of command-line arguments
		space = " "             // Add space after each argument
		fmt.Println(i)
		fmt.Println(os.Args[i])
	}
	fmt.Println(s)
}
