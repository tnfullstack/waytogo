// Echo2 prints its command-line arguments.
package main

import (
   "fmt"
   "os"
)

func main() {
   s, space := "", ""   // short hand declaration of variable s and space with empty string value
   for _, arg := range os.Args[1:] {  // ignore the index, only assign the value to arg variable
      s += space + arg   // cancatenation as normal
      space = " "
   }
   fmt.Println(s)   // out put what store in s
}
