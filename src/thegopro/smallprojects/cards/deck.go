package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

// Create a new type of deck, which is a slice of strings

type deck []string

// Declare a new function
func newDeck() deck {

	cards := deck{}

	cardSuits := []string{"Clubs", "Diamonds", "Hearts", "Spades"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}

	for _, suit := range cardSuits { // "_" means we don't need the index value at this time

		for _, value := range cardValues { // "_" same here

			// inserting values from cardSuits and cardValues to cards array
			cards = append(cards, value+" of "+suit)

		}
	}

	return cards // function return statement

}

// Convert slice (deck) to slice string
func (d deck) toString() string {

	str := strings.Join([]string(d), ", ")
	return str

}

// Save to file
func (d deck) saveToFile(filename string) error {

	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)

}

// Declare a print function with Go's reciver option
func (d deck) print() { // (d, deck) is function receiver on the print() method

	for i, card := range d { // Normal slice iteration

		fmt.Println(i, card) // Print index and slice value

	}
}

// Create new slice of dec with from a file with string of cards
func newDeckFromFile(filename string) deck {

	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	s := strings.Split(string(bs), ",")
	return deck(s)
}

// Shuffle cards un random order
func (d deck) shuffle() {

	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {

		newPosition := r.Intn(len(d) - 1)

		d[i], d[newPosition] = d[newPosition], d[i]

	}
}

// Deal function
func deal(d deck, handSide int) (deck, deck) {

	return d[:handSide], d[handSide:]

}

// Output separator
func separator() {
	fmt.Println("-----------------------------------------------------------")
}
