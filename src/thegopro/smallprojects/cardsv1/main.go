package main

import (
	"fmt"
)

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contact   contactInfo
}

// First main method
/*
func main() {

	// alex := person{"Alex", "Anderson"} //	property declare method one
	// alex := person{firstName: "Alex", lastName: "Anderson"}	// property declare method two
	var alex person
	alex.firstName = "Alex"
	alex.lastName = "Anderson"

	fmt.Println(alex)

}
*/

// Second main method
func main() {

	// jim := person{
	// 	firstName: "Jim",
	// 	lastName:  "Smith",
	// 	contact: contactInfo{
	// 		email:   "jim@email.com",
	// 		zipCode: 95378,
	// 	},
	// }

	// call newPerson to make Jim
	jim := newPerson()

	// Change jim fistName to Jimmy
	jimPointer := &jim
	jimPointer.updateName("Jimmy", "Dude")
	jim.print()

	// Make new person
	chris := newPerson()

	// Change person firstName to Chris
	chrisPointer := &chris
	chrisPointer.updateName("Chris", "Nguyen")
	chris.print()
}

// Func
func (p person) print() {
	fmt.Printf("%+v\n", p)
}

// make new person
func newPerson() person {

	p := person{
		firstName: "Jim",
		lastName:  "Smith",
		contact: contactInfo{
			email:   "jim@email.com",
			zipCode: 95378,
		},
	}

	return p
}

// update name
func (p *person) updateName(newFirstName string, newLastName string) {
	(*p).firstName = newFirstName
	(*p).lastName = newLastName
}
