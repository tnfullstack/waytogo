// channels check if the webside links on the list are up or not, then print statu/error if the link is
// down or up

package main

import (
	"fmt"
	"net/http"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://twitter.com",
		"http://golang.org",
		"http://amazon.com",
		"http://monksedo.com",
		"http://ebay.com",
		"http://test.co",
		"http://tthngd.net",
		"http://udemy.com",
	}

	c := make(chan string)

	for _, link := range links {
		go checkLink(link, c)
		// fmt.Println(<-c)
	}

	for i := 0; i < len(links); i++ {
		fmt.Println(<-c)
	}
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, "might be down!")
		c <- "Might be down I think!"
		return
	}
	fmt.Println(link, "is up!")
	c <- "Yes, its up"
}
