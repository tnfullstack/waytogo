package main

import (
	"fmt"
)

func main() {
	// Method one
	colors := map[string]string{
		"Brown":  "#a64a2b", // Assign value pair
		"Red":    "#ff0000",
		"Orange": "#ffbf00",
		"Yellow": "#ffff00",
	}

	// Method two
	/*
		var colors map[string]string	// Assign 0 value to map
	*/

	// Method three
	/*
		colors := make(map[int]string)

		colors[1] = "#a64a2b"
		colors[2] = "#ff0000"
		colors[3] = "#ffbf00"
		colors[4] = "#ffff00"

		delete(colors, 4)
	*/
	// fmt.Println(colors)

	printMap(colors)
}

func printMap(c map[string]string) {
	for color, hex := range c {
		fmt.Printf("Hex code for %v\n", color+" is "+hex)
	}
}
