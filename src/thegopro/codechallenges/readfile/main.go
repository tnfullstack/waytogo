/*******************************************************************************************************

	readfile.io read the content of the text tile and print its contents ot the terminal

*******************************************************************************************************/
package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	// Read 1 to many command line arguments
	for _, filename := range os.Args[1:] {
		data, err := ioutil.ReadFile(filename) // read data from file
		if err != nil {                        // check if there is any err
			fmt.Fprintf(os.Stderr, "Error: %v\n", err) // print error to console
		} else {
			fmt.Println(string(data)) // convert data to string then print to console
		}
	}
}
