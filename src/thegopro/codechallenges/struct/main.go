package main

import "fmt"

type shape interface {
	getArea() float64
}

type triangle struct {
	base   float64 // I am loss at this step
	height float64
}

type square struct {
	sideLength float64 // same here
}

func main() {
	ta := triangle{base: 20, height: 10}
	sa := square{sideLength: 25}

	printArea(ta)
	printArea(sa)

}

func printArea(s shape) {
	fmt.Println(s.getArea())
}

func (t triangle) getArea() float64 {
	return 0.5 * t.base * t.height

}

func (s square) getArea() float64 {
	return s.sideLength * s.sideLength
}
