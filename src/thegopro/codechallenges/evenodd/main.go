package main

import "fmt"

func main() {
	num := []int{1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20}

	for i := 0; i < len(num); i++ {
		if num[i]%2 == 0 {
			fmt.Println(num[i], " is Even")
		} else {
			fmt.Println(num[i], " is Odd")
		}
	}
}
