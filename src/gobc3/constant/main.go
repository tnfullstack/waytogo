// Constant Expressions

package main

import (
	"fmt"
	"gobc3/package/sep"
	"time"
)

// Basic example of constant variable, declaration, expressions
func main() {

	const min = 1 + 1
	const pi = 3.14
	const version = "2.0.1" + "-beta"
	const debug = !true

	fmt.Println(min, pi, version, debug)

	sep.Cust("Typed Constant")
	typed()

	sep.Cust("Typeless Constant")
	typeless()

	sep.Cust("Typeless example 1")
	typeless1()

	sep.Cust("Default Types")
	defaultType()

	sep.Cust("Time Duration")
	timeDuration()
}

// Typed
func typed() {

	const min int = 42 // min is typed int

	var i int // i is typed int
	i = min   //  i and min are the same type, so no issue here

	fmt.Printf("%d %T\n", i, i) // 42 int

	// var f float64
	// f = min
	// fmt.Printf("%f %T\n", f, f)
}

// Typeless
func typeless() {

	const min = 42              // min is typeless in this declaration
	var i int = min             // i is typed int, the assignment will convert min to type int
	fmt.Printf("%d %T\n", i, i) // 42 int

	var f float64                 // f is typed float64
	f = min                       // min is typeless, the assignment will convert min to type float64
	fmt.Printf("%.2f %T\n", f, f) // 42.00 float64
}

// Typeless
func typeless1() {

	const min = 42

	var i int = min
	var f float64 = min
	var b byte = min
	var j int32 = min
	var r rune = min

	fmt.Printf("%v %T\t %v %T\t %v %T, %v %T %v %T\n", i, i, f, f, b, b, j, j, r, r)
}

// Default Type
func defaultType() {

	const min int32 = 1
	// const min = 1

	max := 5 + min

	fmt.Printf("%d %T\n", max, max)
}

// Time.Duration
func timeDuration() {

	const c = 10
	// i := 10
	t := time.Second * c
	// t := time.Second * time.Duration(i)
	fmt.Println(t)

}
