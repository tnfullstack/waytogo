// Retro Clock Exercise

package main

import (
	"fmt"
	"time"

	"github.com/inancgumus/screen"
)

func main() {

	// Create the place holder type to hole digit 0 - 9
	type placeholder [5]string

	zero := placeholder{
		"███",
		"█ █",
		"█ █",
		"█ █",
		"███",
	}

	one := placeholder{
		"██ ",
		" █ ",
		" █ ",
		" █ ",
		"███",
	}

	two := placeholder{
		"███",
		"  █",
		"███",
		"█  ",
		"███",
	}

	three := placeholder{
		"███",
		"  █",
		" ██",
		"  █",
		"███",
	}

	four := placeholder{
		"█ █",
		"█ █",
		"███",
		"  █",
		"  █",
	}

	five := placeholder{
		"███",
		"█  ",
		"███",
		"  █",
		"███",
	}

	six := placeholder{
		"███",
		"█  ",
		"███",
		"█ █",
		"███",
	}

	seven := placeholder{
		"███",
		"  █",
		"  █",
		"  █",
		"  █",
	}

	eight := placeholder{
		"███",
		"█ █",
		"███",
		"█ █",
		"███",
	}

	nine := placeholder{
		"███",
		"█ █",
		"███",
		"  █",
		"███",
	}

	sep := placeholder{
		"   ",
		" ░ ",
		"   ",
		" ░ ",
		"   ",
	}

	// Store the digits in digits slice
	digits := []placeholder{
		zero, one, two, three, four, five, six, seven, eight, nine,
	}

	// Print each digits per line
	/*
		for i := range digits {
			for j := range digits[0] {
				fmt.Println(digits[i][j])
			}
			fmt.Println()
		}

		fmt.Println()
	*/

	// Print the digits side by side
	for i := range digits[0] {
		for j := range digits {
			fmt.Print(digits[j][i], "  ")
		}
		fmt.Println()
	}

	// Clear the screen to refresh the clock
	screen.Clear()

	for {
		// Move the cusor to the top left corner of the screen
		screen.MoveTopLeft()

		// Get current time
		now := time.Now()

		// Get current hour, minutes, and second
		h, m, s := now.Hour(), now.Minute(), now.Second()

		// fmt.Println(now)
		// fmt.Printf("%v, %v, %v\n", h, m, s)

		// Creat a clock array by getting the digits from digits array
		clock := []placeholder{
			digits[h/10], digits[h%10],
			sep,
			digits[m/10], digits[m%10],
			sep,
			digits[s/10], digits[s%10],
		}

		// Print the clock array

		for i := range clock[0] {
			for j, d := range clock {
				next := (clock[j][i])

				if d == sep && s%2 == 0 {
					next = "   "
				}
				fmt.Print(next, "  ")
			}
			fmt.Println()
		}
		fmt.Println()

		// Delay 1 second before printing a the clock
		time.Sleep(time.Second)
	}
}
