/*
In this example your goal is to mimic the soft text wrapping feature of text editors. For example, when there are 100 characters on a line and if the soft-wrapping is set to 40, an editor may cut the line that goes beyond 40 characters and display the rest of the line in the next line instead.
*/

/*
EXAMPLE
Wrap the given text for 40 characters per line. For example, for the following input, the program should print the folloing input, the program should print the following output.

RULES
* The program should work with Unicode text.package main
* The program should cut the whole word on the next line.

*/

package main

import (
	"fmt"
	"unicode"
)

func main() {

	const text = `
Galaksinin Batı Sarmal Kolu'nun bir ucunda, haritası bile çıkarılmamış ücra bir köşede, gözlerden uzak, küçük ve sarı bir güneş vardır.

Bu güneşin yörüngesinde, kabaca yüz kırksekiz milyon kilometre uzağında, tamamıyla önemsiz ve mavi-yeşil renkli, küçük bir gezegen döner.

Gezegenin maymun soyundan gelen canlıları öyle ilkeldir ki dijital kol saatinin hâlâ çok etkileyici bir buluş olduğunu düşünürler.

	`

	const lineWidth = 40

	var lw int

	for _, w := range text {
		fmt.Printf("%c", w)

		switch lw++; {
		case lw > lineWidth && w != '\n' && unicode.IsSpace(w):
			fmt.Println()
			fallthrough
		case w == '\n':
			lw = 0
		}
	}

}
