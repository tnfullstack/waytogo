/*
	String Exercises
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

func main() {

	// Exercise: Windows Path
	sep.Cust("Windows Path")
	winPath()

	sep.Cust("Print JSON")
	printJSON()

	sep.Cust("Combine string types")
	multypeSting()

	sep.Cust("Count string length")
	strLength()

	sep.Cust("String Echo Bang")
	strEchoBang()

	sep.Cust("Raw Concat")
	rawConcat()

	sep.Cust("Count the chars")
	countChars()

	sep.Cust("Trim It")
	trimIt()

	sep.Cust("Trim Space")
	trimEmptySpace()

	sep.Cust("Trim Right")
	trimRight()
}

// ---------------------------------------------------------
// EXERCISE: Windows Path
//
//  1. Change the following program
//  2. It should use a raw string literal instead
//
// HINT
//  Run this program first to see its output.
//  Then you can easily understand what it does.
//
// EXPECTED OUTPUT
//  Your solution should output the same as this program.
//  Only that it should use a raw string literal instead.
// ---------------------------------------------------------
func winPath() {
	// HINTS:
	// \\ equals to backslash character
	// \n equals to newline character

	path := "c:\\program files\\duper super\\fun.txt\n" +
		"c:\\program files\\really\\funny.png"

	path1 := `
c:\program files\duper super\fun.txt
c:\program files\really\funny.png`

	fmt.Println(path + "\n" + path1)
}

// ---------------------------------------------------------
// EXERCISE: Print JSON
//
//  1. Change the following program
//  2. It should use a raw string literal instead
//
// HINT
//  Run this program first to see its output.
//  Then you can easily understand what it does.
//
// EXPECTED OUTPUT
//  Your solution should output the same as this program.
//  Only that it should use a raw string literal instead.
// ---------------------------------------------------------
func printJSON() {
	// HINTS:
	// \t equals to TAB character
	// \n equals to newline character
	// \" equals to double-quotes character

	// using escape
	json := "\n" +
		"{\n" +
		"\t\"Items\": [{\n" +
		"\t\t\"Item\": {\n" +
		"\t\t\t\"name\": \"Teddy Bear\"\n" +
		"\t\t}\n" +
		"\t}]\n" +
		"}\n"

	// using backquote `backquote`
	json1 := `
	{
		"Items": [{
			"Item": {
				"name": "Teddy Bear"
			}
		}]
	}`

	fmt.Println(json + "\n" + json1)
}

// How to combine string of different types
func multypeSting() {

	fmt.Println("hello" + ", " + "how" + " " + "are you" + " " + "today?")

	fmt.Println(`hello` + `, ` + `how` + ` ` + `are you` + ` ` + "today?")

	eq := "1 + 2 = "
	sum := 1 + 2

	// fmt.Println(eq + sum)
	fmt.Println(eq + strconv.Itoa(sum))

	// eq = true + " " + false
	eq = strconv.FormatBool(true) + " " + strconv.FormatBool(false)
	fmt.Println(eq)

}

// ---------------------------------------------------------
// EXERCISE: Raw Concat
//
//  1. Initialize the name variable
//     by getting input from the command line
//
//  2. Use concatenation operator to concatenate it
//     with the raw string literal below
//
// NOTE
//  You should concatenate the name variable in the correct
//  place.
//
// EXPECTED OUTPUT
//  Let's say that you run the program like this:
//   go run main.go inanç
//
//  Then it should output this:
//   hi inanç!
//   how are you?
// ---------------------------------------------------------

func rawConcat() {
	// uncomment the code below
	name := os.Args[1]

	// replace and concatenate the `name` variable
	// after `hi ` below

	msg := `hi ` + name + `!` + "\n" +
		` how are you?`

	fmt.Println(msg)
}

// ---------------------------------------------------------
// EXERCISE: Count the Chars
//
//  1. Change the following program to work with unicode
//     characters.
//
// INPUT
//  "İNANÇ"
//
// EXPECTED OUTPUT
//  5
// ---------------------------------------------------------
func countChars() {
	// Currently it returns 7
	// Because, it counts the bytes...
	// It should count the runes (codepoints) instead.
	//
	// When you run it with "İNANÇ", it should return 5 not 7.

	name := os.Args[1]
	fmt.Println(name)
	count := utf8.RuneCountInString(name)
	fmt.Println("Number of characters are", count)
}

// ---------------------------------------------------------
// EXERCISE: Trim It
//
//  1. Look at the documentation of strings package
//  2. Find a function that trims the spaces from
//     the given string
//  3. Trim the text variable and print it
//
// EXPECTED OUTPUT
//  The weather looks good.
//  I should go and play.
// ---------------------------------------------------------
func trimIt() {
	msg := `
	
	The weather looks good.
I should go and play.
	`
	msg = strings.Trim(msg, " \t\n") // Trim function allow to specify char to be trimed
	fmt.Println(msg)
}

// Same as above exercise but using TrimSpace function
func trimEmptySpace() {
	msg := `
	
	The weather looks good.
I should go and play.
	`
	msg = strings.TrimSpace(msg)
	fmt.Println(msg)
}

// ---------------------------------------------------------
// EXERCISE: Right Trim It
//
//  1. Look at the documentation of strings package
//  2. Find a function that trims the spaces from
//     only the right-most part of the given string
//  3. Trim it from the right part only
//  4. Print the number of characters it contains.
//
// RESTRICTION
//  Your program should work with unicode string values.
//
// EXPECTED OUTPUT
//  5
// ---------------------------------------------------------
func trimRight() {
	// currently it prints 17
	// it should print 5

	name := "inanç           "
	name = strings.TrimRight(name, " ")
	fmt.Println(name, len(name))
}
