package main

import (
	"fmt"
	"unicode/utf8"
)

// Calculate string length
func strLength() {

	name := "Chris Nguyen"
	count := utf8.RuneCountInString(name)
	fmt.Printf("There are %d letters in %s\n", count, name)
}
