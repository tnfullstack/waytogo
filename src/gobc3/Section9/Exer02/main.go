package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Sort and write items to a file with their ordinals
//
//  Use the previous exercise.
//
//  This time, print the sorted items with ordinals
//  (see the expected output)
//
//
// EXPECTED OUTPUT
//
//   go run main.go
//     Send me some items and I will sort them
//
//   go run main.go orange banana apple
//
//   cat sorted.txt
//     1. apple
//     2. banana
//     3. orange
//
//
// HINTS
//
//   ONLY READ THIS IF YOU GET STUCK
//
//   + You can use strconv.AppendInt function to append an int
//     to a byte slice. strconv contains a lot of functions for appending
//     other basic types to []byte slices as well.
//
//   + You can append individual characters to a byte slice using
//     rune literals (because: rune literal are typeless numerics):
//
//     var slice []byte
//     slice = append(slice, 'h', 'i', ' ', '!')
//     fmt.Printf("%s\n", slice)
//
//     Above code prints: hi !
// ---------------------------------------------------------

func main() {

	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("Enter a list of name to be sorted")
		return
	}

	fmt.Printf("unsorted: %s\n", args)

	sort.Strings(args)
	fmt.Printf("sorted: %s\n", args)

	for i, el := range args {
		fmt.Printf("#%d %v\n", i, el)
	}

	// Create a slice
	sList := make([]byte, 0, 256)

	for i, sl := range args {
		sList = strconv.AppendInt(sList, int64(i+1), 10)
		sList = append(sList, '.', ' ')
		sList = append(sList, sl...)
		sList = append(sList, '\n')
	}

	err := ioutil.WriteFile("sortedList.txt", sList, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
}
