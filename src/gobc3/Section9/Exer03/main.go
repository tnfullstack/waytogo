package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

// ---------------------------------------------------------
// EXERCISE: Find and write the names of subdirectories to a file
//
//  Create a program that can get multiple directory paths from
//  the command-line, and prints only their subdirectories into a
//  file named: dirs.txt
//
//
//  1. Get the directory paths from command-line
//
//  2. Append the names of subdirectories inside each directory
//     to a byte slice
//
//  3. Write that byte slice to dirs.txt file
//
//
// EXPECTED OUTPUT
//
//   go run main.go
//     Please provide directory paths
//
//   go run main.go dir/ dir2/
//
//   cat dirs.txt
//
//     dir/
//             subdir1/
//             subdir2/
//
//     dir2/
//             subdir1/
//             subdir2/
//             subdir3/
//
//
// HINTS
//
//   ONLY READ THIS IF YOU GET STUCK
//
//   + Get all the files in a directory using ioutil.ReadDir
//     (A directory is also a file)
//
//   + You can use IsDir method of a FileInfo value to detect
//     whether a file is a directory or not.
//
//     Check out its documentation:
//
//     go doc os.FileInfo.IsDir
//
//     # or using godocc
//     godocc os.FileInfo.IsDir
//
//   + You can use '\t' escape sequence for indenting the subdirs.
//
//   + You can find a sample directory structure under:
//     solution/ directory
//
// ---------------------------------------------------------

func main() {
	// Get command-line input directories
	dirs := os.Args[1:]

	// Check for valid input
	if len(dirs) < 1 {
		fmt.Println("Usage: command [dir1 dir2]")
		return
	}

	var list []byte

	for _, l := range dirs {
		files, err := ioutil.ReadDir(l)
		if err != nil {
			fmt.Println(err)
			return
		}

		list = append(list, l...)
		list = append(list, '\n')

		for _, f := range files {
			if f.IsDir() {
				list = append(list, '\t')
				list = append(list, f.Name()...)
				list = append(list, '/', '\n')
			}
		}
	}
	fmt.Printf("%s\n", list)

	// Now write the list to file
	err := ioutil.WriteFile("dirs.txt", list, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
}
