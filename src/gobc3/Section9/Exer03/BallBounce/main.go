// Project: Animate a Bouncing Ball

package main

import "fmt"

func main() {

	// Declar constant data for width and height
	const (
		width    = 50
		height   = 10
		space    = ' '
		cellBall = '🥎'
	)

	// Declare a place holder for the cell veriable
	// Rune will be able to hold multiple bytes
	var cell []rune

	board := make([][]bool, width)
	for x := range board {
		board[x] = make([]bool, height)
	}

	board[0][0] = true

	// draw the board
	for y := range board[0] {
		for x := range board {
			cell = space
			if board[x][y] {
				cell = cellBall
			}
			fmt.Print(string(cell), " ")
		}
		fmt.Println()
	}

}
