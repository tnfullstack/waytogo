package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Do Some Calculations
//
//  1. Print the sum of 50 and 25
//  2. Print the difference of 50 and 15.5
//  3. Print the product of 50 and 0.5
//  4. Print the quotient of 50 and 0.5
//  5. Print the remainder of 25 and 3
//  6. Print the negation of `5 + 2`
//
// EXPECTED OUTPUT
//  75
//  34.5
//  25
//  100
//  1
//  -7
// ---------------------------------------------------------
func calculate() {
	var (
		n1, n2, n3 = 50, 25, 3
		a1, a2     = 15.5, 0.5
		b1, b2     = 5, 2
	)

	r1 := n1 + n2
	fmt.Printf("Sum of %d and %d is %d\n", n1, n2, r1)

	r2 := float64(n1) - a1
	fmt.Printf("The difference of %d and %.2f is %.2f\n", n1, a1, r2)

	r3 := float64(n1) * a2
	fmt.Printf("Product of %d and %.2f is %.2f\n", n1, a2, r3)

	r4 := float64(n1) / a2
	fmt.Printf("Quotient of %d and %.2f is %.2f", n1, a2, r4)

	r5 := n2 % n3
	fmt.Printf("Remainder of %d and %d is %d\n", n2, n3, r5)

	r6 := -b1 + -b2
	fmt.Printf("Negation of `%d + %d` is %d\n", b1, b2, r6)
}
