/*
	package sep prints the desk line normaly use as command-line out put separotor
*/

package sep

import (
	"fmt"
	"strings"
)

// Cust = Custom, Cust function allow user to assign custom label to the separator
// Example: sep.Cust("Custom Label")	// ------------Custom Label-------------
func Cust(s string) string {
	fmt.Printf("\n-----------%v----------\n", s)
	return s
}

// Dash = -, Dash function allows user to print a simple ---- line separator
// Exampe: sep.Dash()
func Dash(n int) int {
	fmt.Println(strings.Repeat("-", n))
	return n
}
