package printer

import "fmt"

// This is an exported function
func Hello() {
	fmt.Println("Hello Go Package!, Testing packages in Go.")
}
