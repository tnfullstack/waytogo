/*
	Testing Go package
*/

package goversion

import "runtime"

func Version() string {
	return runtime.Version()
}
