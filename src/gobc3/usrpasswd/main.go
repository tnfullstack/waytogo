// User name and password check example

package main

import (
	"fmt"
	"os"
)

const (
	usage  = `usage: [username] [password]`
	pwerr  = `Invalid password for`
	usrerr = `Access denied for`
	pass   = `Access granted to`
)

func main() {
	const (
		usr = "chris"
		pw  = "1881"
	)

	args := os.Args

	if len(args) != 3 {
		fmt.Println(usage)
		return
	}

	u, p := args[1], args[2]

	if u == usr && p == pw {
		fmt.Println(pass, u)
	} else if u == usr && p != pw {
		fmt.Println(pwerr, u)
	} else {
		fmt.Println(usrerr, u)
	}
}
