package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Convert and Fix #2
//
//  Fix the code by using the conversion expression.
//
// EXPECTED OUTPUT
//  10.5
// ---------------------------------------------------------

func convertAndFix2() {

	// This is my solution, if the gold of the print out is 10.5
	a, b := 10, 5.5
	fmt.Println(float64(a) + b - 5.)

	// Not sure if this is a good example from the solution
	// a, b := 10, 5.5
	// a = int(b)
	// fmt.Println(float64(a) + b)
}
