package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Convert and Fix
//
//  Fix the code by using the conversion expression.
// 	a, b := 10, 5.5
// 	fmt.Println(a + b)	// Complile arror
//
// EXPECTED OUTPUT
//  15.5
// ---------------------------------------------------------
func convertAndFix() {

	a, b := 10, 5.5
	fmt.Println(float64(a) + b)

}
