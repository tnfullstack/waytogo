/*
	Variables with zero value in GO
*/

package main

import "fmt"

func main() {
	var speed int    // default zero value
	var heat float64 // default zero value
	var off bool     // zero value of bool = false
	var brand string // zero value in string = ""

	fmt.Println(speed)
	fmt.Println(heat)
	fmt.Println(off)
	fmt.Printf("%q\n", brand)
	fmt.Println(brand)
}
