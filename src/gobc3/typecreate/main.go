/*
	Go Type System
*/

package main

import (
	"fmt"
	"gobc3/typecreate/weights"
)

type gram float64
type ounce float64

func main() {

	var g gram = 1000
	var o ounce

	o = ounce(g) * 0.035274

	fmt.Printf("%g gram is %.2f ounces\n", g, o)

	example01()

	aliasType()
}

// Custom type Example 1
func example01() {
	type (
		// Gram's underlying type is int
		Gram int

		// Kilogram's underlying type is int
		Kilogram Gram

		// Ton's underlying type is int
		Ton Kilogram
	)

	var (
		salt   = weights.Gram(100)
		apples = weights.Kilogram(5)
		truck  = weights.Ton(10)
	)
	fmt.Printf("Salt: %d %T, apples: %d %T, truck: %d %T\n", salt, salt, apples, apples, truck, truck)
}

// Alias Type Example
func aliasType() {

	var (
		byteVal  byte
		uint8Val uint8
		intVal   int
	)
	uint8Val = byteVal

	var (
		runeVal  rune
		int32Val int32
	)
	runeVal = int32Val

	fmt.Printf("%T\t%T\t%T\t%T\t%T\n", byteVal, uint8Val, intVal, runeVal, int32Val)

}
