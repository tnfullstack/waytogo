/*
	Logical AND
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

main func() {

	sep.Cust("Logical AND")
	logicalAnd() // obvious call to logicalAnd() function
	sep.Cust("Logical OR")
	logicalOr() // Same here
	sep.Cust("Logical NOT")
	logicalNOT()
}

// Just practice basic function
func logicalAnd() {

	speed := 100                         // Set speed to 100
	check := speed >= 40 && speed <= 55  // save the result of the logical comparison to check
	fmt.Println("Within limits?", check) // print out bool value

	speed -= 50                          // Slow down the speed
	check = speed >= 40 && speed <= 55   // check the limits again
	fmt.Println("within limits?", check) // print the bool value again

}

// Another function
func logicalOr() {

	color := "red" // Set color to "red"

	check := color == "red" || color == "dark red" // Save the bool value to check
	fmt.Println("Reddish color?", check)           // print out the bool value

	color = "dark red" // change color to "dark red"
	check = color == "red" || color == "dark red"
	fmt.Println("Reddish color?", check)

	check = color == "greenish" || color == "light green"
	fmt.Println("Reddish color?", check)

}

// The NOT operator
func logicalNOT() {
	var sw bool

	sw = !sw

	fmt.Printf("Switch is %t\n", sw)
}
