package main

import (
	"fmt"
	"os"
)

// ---------------------------------------------------------
// EXERCISE: Greet 5 People
//
//  Greet 5 people this time.
//
//  Please do not copy paste from the previous exercise!
//
// RESTRICTION
//  This time do not use variables.
//
// INPUT
//  bilbo balbo bungo gandalf legolas
//
// EXPECTED OUTPUT
//  There are 5 people!
//  Hello great bilbo !
//  Hello great balbo !
//  Hello great bungo !
//  Hello great gandalf !
//  Hello great legolas !
//  Nice to meet you all.
// ---------------------------------------------------------
func greet5people() {

	n := os.Args

	fmt.Printf("There are %v people!\n", len(n)-1)
	fmt.Printf("Hello great %v!\n", n[1])
	fmt.Printf("Hello great %v!\n", n[2])
	fmt.Printf("Hello great %v!\n", n[3])
	fmt.Printf("Hello great %v!\n", n[4])
	fmt.Printf("Hello great %v!\n", n[5])

	fmt.Println("Nice to meet you all.")

}
