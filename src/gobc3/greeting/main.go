/*
	Greeting program
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
	"os"
)

func main() {
	var name string
	name = os.Args[1]
	fmt.Println("Hello,", name, "!")
	name, age := "Mike", 2019

	fmt.Println("My name is", name)
	fmt.Println("My age is", age)

	sep.Cust("Greeting more people")
	greetMore()

	sep.Cust("Greeting 5 people")
	greet5people()
}
