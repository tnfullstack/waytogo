/*
	Feet to Meters codin exercises
*/

package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

const usage = `
Feet to Meters
--------------
This program converts feet into meters.

Usage:
$ command [feet]

# Example
$ feettometer 100
`

// Everyone know that the program start here func main()
func main() {

	// feetToMetters()
	feetToMetters1()

	test()
}

// First code base example
func feetToMetters() {

	const (
		ftm = 0.3048
		fty = ftm / 0.9144
	)

	if len(os.Args) != 2 {
		fmt.Println(strings.TrimSpace(usage))
		return
	}

	arg := os.Args[1]
	feet, err := strconv.ParseFloat(arg, 64)

	if err != nil {
		fmt.Printf("ERROR: %q is not a number.\n", arg)
		return
	}

	meters := feet * ftm
	yards := math.Round(feet * fty)

	fmt.Printf("%.2f feet is %.2f meters and %.2f yards\n", feet, meters, yards)
}

// Refactor code with veriable scope
func feetToMetters1() {

	const (
		ftm = 0.3048
		fty = ftm / 0.9144
	)

	var (
		feet float64
		err  error
	)

	if a := os.Args; len(a) != 2 {
		// a is only availabel to this code block
		fmt.Println(usage)
	} else if feet, err = strconv.ParseFloat(a[1], 64); err != nil {
		// a, and n are available in side if code block
		fmt.Printf("Cannot convert %q.\n", a[1])
	} else {
		// a, and n are also accessible here, because else is still belong to if code block
		meters := feet * ftm
		yards := math.Round(feet * fty)
		fmt.Printf("%.2f feet is %.2f meters and %.2f yards\n", feet, meters, yards)
	}
}

func test() {
	if d, err := time.ParseDuration("1h10s"); err == nil {
		fmt.Println(d)
	}

	done := false
	if done := true; done {
		fmt.Println(done)
	}
	fmt.Println(done)
}
