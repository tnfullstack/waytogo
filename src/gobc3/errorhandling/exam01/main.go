// Error Handling Example

package main

import (
	"fmt"
	"os"
	"strconv"
)

const (
	errnum = "is not and number."
)

func main() {

	age := os.Args[1]

	n, err := strconv.Atoi(age)

	if err != nil {
		fmt.Println("ERROR:", age, errnum)
		return
	}
	fmt.Printf("Success: Converted %q to %d.\n", age, n)
}
