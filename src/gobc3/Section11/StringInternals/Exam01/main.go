package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {

	const word = "gõkyưzư"
	bword := []byte(word)

	fmt.Println(utf8.RuneCount(bword), len(word), len(string(word[1])))

	fmt.Printf("String print witn s : %s\n", bword)
	fmt.Printf("Binary print with b : %b\n", bword)
	fmt.Printf("Hex print with x : % x\n", bword)
	fmt.Printf("Dec print with d : % d\n", bword)
}
