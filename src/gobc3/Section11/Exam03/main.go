// Learning the basic of bytes, runes and strings
// Exmaple 03

package main

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

func main() {
	str := "Thành Nguyễn"

	bytes := []byte(str)

	runes := []rune(str)

	fmt.Printf("Printf with b %b\n", bytes)
	fmt.Printf("Printf with c %c\n", bytes)
	fmt.Printf("Printf with d %d\n", bytes)
	fmt.Printf("Printf with x % x\n", bytes)

	fmt.Printf("%d bytes\n", len(str))

	fmt.Printf("%d runes\n", utf8.RuneCountInString(str))

	fmt.Printf("%d runes\n", utf8.RuneCount(bytes))

	fmt.Printf("%s\n", strings.Repeat("-", 50))

	for i, r := range str {
		fmt.Printf("str[%2d] = % -12x = %q\n", i, string(r), r)
	}

	fmt.Printf("%d\n", runes)
}
