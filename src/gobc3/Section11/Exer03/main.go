package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Print the runes
//
//  1. Loop over the "console" word and print its runes one by one,
//     in decimals, hexadecimals and binary.
//
//  2. Manually put the runes of the "console" word to a byte slice, one by one.
//
//     As the elements of the byte slice use only the rune literals.
//
//     Print the byte slice.
//
//  3. Repeat the step 2 but this time, as the elements of the byte slice,
//     use only decimal numbers.
//
//  4. Repeat the step 2 but this time, as the elements of the byte slice,
//     use only hexadecimal numbers.
//
//
// EXPECTED OUTPUT
//   Run the solution to see the expected output.
// ---------------------------------------------------------

func main() {
	const word = "console"

	for _, el := range word {
		fmt.Printf("%c\n", el)
		fmt.Printf("\tdecimal: %[1]d\n", el)
		fmt.Printf("\thex    : 0x%[1]x\n", el)
		fmt.Printf("\tbinary : 0b%08[1]b\n", el)
	}

	// Print the word manually using runes
	fmt.Printf("With rune		: %x\n", string([]byte{'c', 'o', 'n', 's', 'o', 'l', 'e'}))

	// Print the word manually using decimals
	fmt.Printf("With decimals		: %s\n", string([]byte{99, 111, 110, 115, 111, 108, 101}))

	// Print the word manually using hexadecimals
	fmt.Printf("With hexadeimals	: %s\n", string([]byte{0x63, 0x6f, 0x6e, 0x73, 0x6f, 0x6c, 0x65}))

	// Print the word manually with binary
	fmt.Printf("With binary		: %s\n", string([]byte{0b01100011, 0b01101111, 0b01101110, 0b01110011, 0b01101111, 0b01101100, 0b01100101}))

}

/*
func main() {
	const word = "console"

	for _, w := range word {
		fmt.Printf("%c\n", w)
		fmt.Printf("\tdecimal: %[1]d\n", w)
		fmt.Printf("\thex    : 0x%[1]x\n", w)
		fmt.Printf("\tbinary : 0b%08[1]b\n", w)
	}

	// print the word manually using runes
	fmt.Printf("with runes       : %s\n",
		string([]byte{'c', 'o', 'n', 's', 'o', 'l', 'e'}))

	// print the word manually using decimals
	fmt.Printf("with decimals    : %s\n",
		string([]byte{99, 111, 110, 115, 111, 108, 101}))

	// print the word manually using hexadecimals
	fmt.Printf("with hexadecimals: %s\n",
		string([]byte{0x63, 0x6f, 0x6e, 0x73, 0x6f, 0x6c, 0x65}))
}
*/
