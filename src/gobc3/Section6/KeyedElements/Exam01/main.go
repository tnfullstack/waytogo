// Keyed Elements Example 1

package main

import "fmt"

func main() {

	const (
		ETH = 1 + iota // Using iota as increment for index value and assign to Keyed Elements
		WAN            // start from 0, and increament by 1, therefore ETH = 0, WAN = 1
	)

	fmt.Println(ETH, WAN)

	rates := [...]float64{
		ETH: 25.5,  // ethereum
		WAN: 120.5, // wanchain
	}

	fmt.Printf("rates : %#v\n", rates)
	fmt.Printf("1 BTC is %g ETH\n", rates[ETH])
	fmt.Printf("1 BTC is %g WAN\n", rates[WAN])
}
