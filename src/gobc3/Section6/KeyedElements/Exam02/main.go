// Keyed Elements Example 2

package main

import "fmt"

func main() {

	const (
		ETH = 9 - iota // Using iota as increment for index value and assign to Keyed Elements
		WAN            // start from 0, and increament by 1, therefore ETH = 0, WAN = 1
		ICX
	)

	fmt.Printf("ETH = %v; WAN = %v; ICX = %v\n", ETH, WAN, ICX)

	rates := [...]float64{
		ETH: 25.5,  // ethereum
		WAN: 120.5, // wanchain
		ICX: 20.3,
	}

	fmt.Printf("rates : %#v\n", rates)

	fmt.Printf("1 BTC is %g ETH\n", rates[ETH])
	fmt.Printf("1 BTC is %g WAN\n", rates[WAN])
}
