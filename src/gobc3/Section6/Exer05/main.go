package main

import (
	"fmt"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Refactor to Ellipsis
//
//  1. Use the 03-array-literal exercise
//
//  2. Refactor the length of the array literals to ellipsis
//
//    This means: Use the ellipsis instead of defining the array's length
//                manually.
//
// EXPECTED OUTPUT
//   The output should be the same as the 03-array-literal exercise.
// ---------------------------------------------------------

func main() {
	names := [...]string{"Son", "Thai", "Hung"}

	data := [...]uint8{'H', 'E', 'L', 'L', 'O'}

	distances := [...]int{63, 1255, 1700, 2280, 2891}

	ratios := [...]float64{3.14145}

	alives := [...]bool{true, false, true, false}

	// Don't do this
	// zero := [0]byte{}

	// Do this when you don't have assign elements
	var zero []byte

	sep := "\n" + strings.Repeat("=", 20) + "\n"

	fmt.Print("names", sep)
	for i, n := range names {
		fmt.Printf("names[%d]: %q\n", i, n)
	}

	fmt.Print("\ndistances", sep)

	for i, d := range distances {
		fmt.Printf("distances[%d]: %d\n", i, d)
	}

	fmt.Print("\ndata", sep)

	for i := 0; i < len(data); i++ {
		fmt.Printf("data[%d]: %d\n", i, data[i])
	}

	fmt.Print("\nratios", sep)

	for i, r := range ratios {
		fmt.Printf("ratios[%d]: %.2f\n", i, r)
	}

	fmt.Print("\nalives", sep)

	for i, a := range alives {
		fmt.Printf("alives[%d]: %t\n", i, a)
	}

	fmt.Print("\nzero", sep)

	for i, z := range zero {
		fmt.Printf("zero[%d]: %d\n", i, z)
	}
}
