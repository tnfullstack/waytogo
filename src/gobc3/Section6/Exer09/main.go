package main

import (
	"fmt"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Wizard Printer
//
//   In this exercise, your goal is to display a few famous scientists
//   in a pretty table.
//
//   1. Create a multi-dimensional array
//   2. In each inner array, store the scientist's name, lastname and his/her
//      nickname
//   3. Print their information in a pretty table using a loop.
//
// EXPECTED OUTPUT
//   First Name      Last Name       Nickname
//   ==================================================
//   Albert          Einstein        time
//   Isaac           Newton          apple
//   Stephen         Hawking         blackhole
//   Marie           Curie           radium
//   Charles         Darwin          fittest
// ---------------------------------------------------------

func main() {
	sep1 := strings.Repeat("=", 42)
	sep2 := strings.Repeat("-", 42)

	// heading := [...]string{"Frist Name", "Last Name", "Nickname"}

	wizards := [...][3]string{
		{"First Name", "Last Name", "Nickname"},
		{"Albert", "Einstein", "time"},
		{"Isaac", "Newton", "apple"},
		{"Stephen", "Hawking", "blackhole"},
		{"Marie", "Curie", "radium"},
		{"Charles", "Darwin", "fittest"},
	}

	fmt.Println()
	/*
		for _, h := range heading {
			fmt.Printf("%v\t", h)
		}
	*/

	// fmt.Println(sep1)

	/*
		for i, w := range wizards {
			for _, n := range w {
				fmt.Printf("%-15s", n)
			}

			if i == 0 {
				fmt.Printf("\n%v", sep1)
			}

			fmt.Println()
		}
	*/

	// Refactor block
	for i, w := range wizards {
		fmt.Printf("%-15s %-15s %-15s\n", w[0], w[1], w[2])

		if i == 0 {
			fmt.Println(sep1)
		}
	}

	fmt.Printf("%v\n", sep2)
	fmt.Println()
}
