package main

import (
	"fmt"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Refactor to Array Literals
//
//  1. Use the 02-get-set-arrays exercise
//
//  2. Refactor the array assignments to array literals
//
//    1. You would need to change the array declarations to array literals
//
//    2. Then, you would need to move the right-hand side of the assignments,
//       into the array literals.
//
// EXPECTED OUTPUT
//   The output should be the same as the 02-get-set-arrays exercise.
// ---------------------------------------------------------

func main() {
	names := [3]string{"Son", "Thai", "Hung"}

	data := [5]uint8{'H', 'E', 'L', 'L', 'O'}

	distances := [5]int{63, 1255, 1700, 2280, 2891}

	ratios := [1]float64{3.14145}

	alives := [4]bool{true, false, true, false}

	// Don't do this
	// zero := [0]byte{}

	// Do this when you don't have assign elements
	var zero [0]byte

	sep := "\n" + strings.Repeat("=", 20) + "\n"

	fmt.Print("names", sep)
	for i, n := range names {
		fmt.Printf("names[%d]: %q\n", i, n)
	}

	fmt.Print("\ndistances", sep)

	for i, d := range distances {
		fmt.Printf("distances[%d]: %d\n", i, d)
	}

	fmt.Print("\ndata", sep)

	for i := 0; i < len(data); i++ {
		fmt.Printf("data[%d]: %d\n", i, data[i])
	}

	fmt.Print("\nratios", sep)

	for i, r := range ratios {
		fmt.Printf("ratios[%d]: %.2f\n", i, r)
	}

	fmt.Print("\nalives", sep)

	for i, a := range alives {
		fmt.Printf("alives[%d]: %t\n", i, a)
	}

	fmt.Print("\nzero", sep)

	for i, z := range zero {
		fmt.Printf("zero[%d]: %d\n", i, z)
	}
}
