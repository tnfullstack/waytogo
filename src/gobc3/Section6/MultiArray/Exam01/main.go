// Multi-Dimentional Array Example 1

package main

import "fmt"

func main() {

	/*
		// Basic array
		student1 := [3]float64{5, 6, 9}
		student2 := [3]float64{9, 5, 8}

		var sum float64

		sum += student1[0] + student1[1] + student1[2]
		sum += student2[0] + student2[1] + student2[2]

		fmt.Println(sum / float64(len(student1)*2))
	*/

	/*
		// Multi-dimentional array
		students := [2][3]float64{
			[3]float64{5, 6, 9},
			[3]float64{9, 5, 8},
		}

		var sum float64

		sum += students[0][0] + students[0][1] + students[0][2]
		sum += students[1][0] + students[1][1] + students[1][2]

		// Find total number of student the multi-dimention array
		const N = float64(len(students) * len(students[0]))

		// Print the average score for all students
		fmt.Printf("Average grade: %g\n", sum/N)
	*/

	// ---------------- Refactor the code above ---------------------

	students := [...][3]float64{
		{5, 6, 9},
		{9, 5, 8},
	}

	var sum float64

	for i, arr := range students {
		fmt.Println(i, arr)
		for _, g := range arr {
			sum += g
		}
	}

	const N = float64(len(students) * len(students[0]))
	fmt.Printf("Total Score: %g\n", sum)
	fmt.Printf("Average Score: %g\n", sum/N)

}
