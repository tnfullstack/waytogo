// Campare Array Example

package main

import "fmt"

func main() {

	var (
		blue = [3]int{5, 9, 3}
		red  = [3]int{5, 9, 2}
	)

	fmt.Printf("Blue bookcase : %v\n", blue)
	fmt.Printf("Red bookcase : %v\n", red)

	fmt.Println("Are they equal?", blue == red)

}
