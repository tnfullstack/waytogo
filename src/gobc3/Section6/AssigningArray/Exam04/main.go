// Assigning Array Example 3

package main

import "fmt"

func main() {
	prev := [3]string{"Kafka's Revenge", "Stay Golden", "Everythingship"}

	var books [4]string

	for i, p := range prev {
		books[i] = p + " 2nd Ed."
	}

	fmt.Printf("Last year: \n%#v\n", prev)
	fmt.Printf("This year: \n%#v\n", books)

	// Add a new book to books array
	books[3] = "Awesomeness"

	fmt.Printf("This year: \n%#v\n", books)
}
