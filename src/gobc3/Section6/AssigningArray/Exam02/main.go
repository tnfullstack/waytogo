// Assingning Array Example 2

package main

import "fmt"

func main() {

	blue := [3]int{5, 12, 9}
	red := [5]int{3, 5, 9}
	red = blue // Go will not assign array with different type

	fmt.Printf("Blue : %v\n", blue)
	fmt.Printf("Red : %v\n", red)

}
