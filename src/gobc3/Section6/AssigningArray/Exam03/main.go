// Assigning Array Example 3

package main

import "fmt"

func main() {
	prev := [3]string{"Kafka's Revenge", "Stay Golden", "Everythingship"}

	books := prev

	for i := range books {
		books[i] += " 2nd Ed."
	}

	fmt.Printf("Last year: \n%#v\n", prev)
	fmt.Printf("This year: \n%#v\n", books)

}
