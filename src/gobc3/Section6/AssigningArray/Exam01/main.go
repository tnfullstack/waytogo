// Assingning Array Example

package main

import "fmt"

func main() {

	blue := [3]int{5, 12, 9}
	red := blue // Go will make a duplicate copy of blue to create red array

	fmt.Printf("Blue : %v\n", blue)
	fmt.Printf("Red : %v\n", red)

	fmt.Println("Are they equal? ", blue == red)

	blue[0] = 20 // Change the value of element in blue array will not reflex in red array
	fmt.Printf("Blue : %v\n", blue)
	fmt.Printf("Red : %v\n", red)

	fmt.Println("Are they equal? ", blue == red)

}
