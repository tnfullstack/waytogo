// Array Literal Example 3 - Refactor the previous examples

package main

import "fmt"

func main() {

	const (
		winter = 1
		summer = 3
		year   = winter + summer
	)

	// var books [year]string

	// books[0] = "Kafka's Revenge"
	// books[1] = "Stay Golden"
	// books[2] = "Everythingship"
	// books[3] = books[0] + " 2nd Edition"

	books := [year]string{
		"Kafka's Revenge",
		"Stay Golden",
		"Everythingship",
	}

	books[3] = "Kafka's Revenge 2nd Edition"

	fmt.Printf("books : %#v\n", books)
}
