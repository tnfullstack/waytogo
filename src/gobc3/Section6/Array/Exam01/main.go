// How to create array and work with array

package main

import "fmt"

func main() {
	// var books [4]string
	// var books [1 + 3]string
	const (
		winter = 1
		summer = 3
		year   = winter + summer
	)

	var books [year]string // Set books array's length using value from year variable

	// fmt.Printf("books : %T\n", books)
	// fmt.Println("books :", books)
	// fmt.Printf("books : %q\n", books)
	// fmt.Printf("books : %#v\n", books)

	books[0] = "Kafka's Revenge"
	books[1] = "Stay Golden"
	books[2] = "Everythingship"
	books[3] = books[0] + " Second Edition"

	fmt.Printf("books : %#v\n", books)

	var (
		wBooks [winter]string
		sBooks [summer]string
	)

	wBooks[0] = books[0]

	for i, v := range sBooks {
		sBooks[i] = books[i+1]
		fmt.Println("value of v is", v)
	}

	fmt.Printf("\nWinter books : %#v\n", wBooks)
	fmt.Printf("\nSummer Books : %#v\n", sBooks)

	var published [len(books)]bool // new published array to track the published books

	published[0] = true
	published[len(books)-1] = true

	for i, ok := range published {
		if ok {
			fmt.Println(i, ok)
		}
	}
}
