// Exercise - Mood

package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	args := os.Args[1:]

	if len(args) != 1 {
		fmt.Println("Please enter your name")
		return
	}

	name := args[0]

	moods := [...]string{
		"sad 😞",
		"good 👍",
		"bad ☹️",
		"terrible 😩",
		"happy 😀",
		"awesome 😎",
	}

	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(len(moods))
	fmt.Println(n)
	fmt.Printf("%v feels %v\n", name, moods[n])
}
