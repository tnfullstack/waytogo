/*
	Working with type inference
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	sep.Cust("Full syntax example")
	example1()

	sep.Cust("Without the type keyword")
	example2()

	sep.Cust("Short-hand writing")
	example3()

	sep.Cust("Multiple Short-hand declaration")
	example4()

}

// Go supports full syntax declaration with the type keyword
func example1() {

	// variable declaration example 1
	var name string = "Carl"

	var isScientist bool = true

	var age int = 62

	var degree float64 = 5.

	fmt.Println(name, isScientist, age, degree)

}

// Without the type keyword
func example2() {

	var name = "Carl"

	var isScientist = true

	var age = 62

	var degree = 5.

	fmt.Println(name, isScientist, age, degree)

}

// Shorthand variable declaration
func example3() {

	name := "Carl"

	isScientist := true

	age := 62

	degree := 5.

	fmt.Println(name, isScientist, age, degree)

}

// Multiple short declaration
func example4() {

	safe, speed := true, 55
	fmt.Println(safe, speed)

	firstName, lastName := "Nikola", "Tesla"
	fmt.Println(firstName, lastName)

	birth, death := 1856, 1955
	fmt.Println(birth, death)

	degree, ratio, heat := 10.55, 30.5, 20.
	fmt.Println(degree, ratio, heat)

	nFiles, valid, msg := 10, true, "hello"
	fmt.Println(nFiles, valid, msg)

}
