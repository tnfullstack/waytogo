/*
	Learn the basics of os.Args
*/

package main

import (
	"fmt"
	"os"
)

func main() {

	args := os.Args

	fmt.Printf("You have entered %v\n", args[1:])

	fmt.Println("Number of arguments:", len(args)-1)
}
