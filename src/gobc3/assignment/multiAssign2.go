package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Multi Assign #2
//
//  1. Assign the correct values to the variables
//     to match to the EXPECTED OUTPUT below
//
//  2. Print the variables
//
// HINT
//  Use multiple Println calls to print each sentence.
//
// EXPECTED OUTPUT
//  Air is good on Mars
//  It's true
//  It is 19.5 degrees
// ---------------------------------------------------------
func multiAssign2() {
	var (
		msg  = "good"
		test = true
		temp = 19.5
	)

	fmt.Printf("Air on Mar is %v\n", msg)
	fmt.Printf("It is %v\n", test)
	fmt.Printf("It is %v\n", temp)

}
