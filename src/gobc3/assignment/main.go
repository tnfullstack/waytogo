/*
	Assignment Exercises
*/

package main

import (
	"gobc3/package/sep"
)

func main() {

	sep.Cust("Make it Blue")
	mkBlue()

	sep.Cust("Variables to Variables")
	varToVar()

	sep.Cust("Assign with Expression")
	assignWithExpression()

	sep.Cust("Calculate Rectangle's Perimeter")
	recPerimeter()

	sep.Cust("Multi-assign")
	multiAssign()

	sep.Cust("Multi-assign 2")
	multiAssign2()

	sep.Cust("Multi-Short")
	multiShort()

	sep.Cust("Swapper")
	swapper()

	sep.Cust("Swapper 2")
	swapper2()

	sep.Cust("Discard The File")
	discardFile()

}
