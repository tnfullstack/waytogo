// ---------------------------------------------------------
// EXERCISE: Discard The File
//
//  1. Print only the directory using `path.Split`
//
//  2. Discard the file part
//
// RESTRICTION
//  Use short declaration
//
// EXPECTED OUTPUT
//  secret/
// ---------------------------------------------------------

package main

import (
	"fmt"
	"path/filepath"
)

func discardFile() {

	paths := "secret/file.txt"

	dir, _ := filepath.Split(paths)

	fmt.Println(dir)
}
