// ---------------------------------------------------------
// EXERCISE: Multi Short Func
//
// 	1. Declare two variables using multiple short declaration syntax
//
//  2. Initialize the variables using `multi` function below
//
//  3. Discard the 1st variable's value in the declaration
//
//  4. Print only the 2nd variable
//
// NOTE
//  You should use `multi` function
//  while initializing the variables
//
// EXPECTED OUTPUT
//  4
// ---------------------------------------------------------

package main

import "fmt"

func multiShort() {

	num1, num2 := multi()

	_ = num1

	fmt.Println(num2)

}

// multi-assign is a function that returns multipe int values
func multi() (int, int) {
	return 5, 9
}
