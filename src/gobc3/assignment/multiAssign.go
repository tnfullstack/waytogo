// ---------------------------------------------------------
// EXERCISE: Multi Assign
//
//  1. Assign "go" to `lang` variable
//     and assign 2 to `version` variable
//     using a multiple assignment statement
//
//  2. Print the variables
//
// EXPECTED OUTPUT
//  go version 2
// ---------------------------------------------------------

package main

import "fmt"

func multiAssign() {

	lang, version := "Go", 1.42

	fmt.Printf("Language: %v, version %v\n", lang, version)
}
