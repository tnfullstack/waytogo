// ---------------------------------------------------------
// EXERCISE: Swapper
//
//  1. Change `color` to "orange"
//     and `color2` to "green" at the same time
//
//     (use multiple-assignment)
//
//  2. Print the variables
//
// EXPECTED OUTPUT
//  orange green
package main

import "fmt"

func swapper() {

	color, color2 := "Orange", "Green"

	fmt.Printf("Before swap %s, %s\n", color, color2)

	color, color2 = color2, color

	fmt.Printf("After swap %s, %s\n", color, color2)

}
