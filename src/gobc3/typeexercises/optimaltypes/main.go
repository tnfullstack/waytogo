/*
	Types Exercises
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	sep.Cust("Optimal Types")
	optimalTypes()

	sep.Cust("Fix The Type Problem")
	fixTheType()

}

// ---------------------------------------------------------
// EXERCISE: Optimal Types
//
//  1. Choose the optimal data types for the given situations.
//  2. Print them all
//  3. Try converting them to lesser data types.
//     For example try converting int64 variable to int32.
//     Then observe the result.
//     Search the web why the result is so?
//
// NOTE
//  This is just an exercise for teaching you different
//  data types. Do not apply it to the real-life.
//
//  As I said in the lectures that, premature optimization
//  is not a good thing.
// ---------------------------------------------------------
func optimalTypes() {
	// DONT FORGET: There are also unsigned data types.
	//              (For positive numbers)

	// DO NOT USE: int data type
	// Use only the ones with the bitsizes

	// ---

	// an english letter (search web for: ascii control code)
	var b byte = 'B'
	fmt.Println("An english leter:", b)

	// a non-english letter (search web for: unicode codepoint)
	var j rune = '本'
	fmt.Println("A unicode leter:", j)

	// a year in 4 digits like 2040
	var y uint16 = 2020
	fmt.Println("Year", y)

	// a month in 2 digits: 1 to 12
	var m uint8 = 12
	fmt.Println("Months", m)

	// the speed of the light
	var sol rune = 299792458
	fmt.Println("Speed of light", sol)

	// angle of a circle
	var abc float32 = 55.9
	fmt.Println("Angle", abc)
	// PI number: 3.141592653589793
	var PI float32 = 3.141592653589793
	fmt.Println("PI", PI)
}

// ---------------------------------------------------------
// EXERCISE: The Type Problem
//
//  Solve the data type problem in the program.
//
// EXPECTED OUTPUT
//  width: 265 height: 265
//  are they equal? true
// ---------------------------------------------------------

func fixTheType() {
	// FIX THIS:
	// Change the following data types to the correct
	// data types where appropriate.
	var (
		// width  uint8	// Original assignment

		width  uint16 // Fix width uint16 to allow width to hold 255 + 10 = 265
		height uint16 // Now both width and height are the same type
	)

	// DONT TOUCH THIS:
	width, height = 255, 265
	width += 10
	fmt.Printf("width: %d height: %d\n", width, height)

	// UNCOMMENT THIS:
	fmt.Println("are they equal?", width == height)
}
