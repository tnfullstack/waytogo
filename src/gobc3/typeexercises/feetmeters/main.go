/*
	Feet to Meters codin exercises
*/

package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

const usage = `
Feet to Meters
--------------
This program converts feet into meters.

Usage:
$ command feet > enter
`

// Everyone know that the program start here func main()
func main() {

	firstCodeBase()

	refactor()

}

// First code base example
func firstCodeBase() {
	if len(os.Args) < 2 {
		fmt.Println(strings.TrimSpace(usage))
		return
	}
	arg := os.Args[1]

	feet, _ := strconv.ParseFloat(arg, 64)

	meters := feet * 0.3048

	fmt.Printf("%g feet is %.2f meters.\n", feet, meters)
}

// ---------------------------------------------------------
// EXERCISE: Refactor Feet to Meter
//
//  Remember the feet to meters program?
//  Now, it's time to refactor it.
//  Define your own Feet and Meters types.
//
//  Follow the steps inside the code.
// ---------------------------------------------------------

func refactor() {
	// ----------------------------
	// 1. Define Feet and Meters types below
	//    Their underlying type can be float64
	// ...
	type (
		Feet   float64
		Meters float64
	)

	// ----------------------------
	// 2. UNCOMMENT THE CODE BELOW THEN DON'T TOUCH IT
	var (
		feet   Feet
		meters Meters
	)

	// ----------------------------
	// 3. Get feet value from the command-line
	// 4. Convert it to an float64 first using ParseFloat
	// 5. Then, convert it into a Feet type
	// ... TYPE YOUR CODE HERE
	f, _ := strconv.ParseInt(os.Args[1], 10, 64)
	feet = Feet(f)
	fmt.Printf("%d %T\n", f, f)

	// ----------------------------
	// 6. Uncomment the code below
	// 7. And, convert the expression to Meters type

	meters = Meters(feet * 0.3048)

	// ----------------------------
	// 8. UNCOMMENT THE CODE BELOW
	fmt.Printf("%g feet is %.2f meters.\n", feet, meters)
}
