/*
	Data Type Examples
*/

// ---------------------------------------------------------
// EXERCISE: Print the literals
//
//  1. Print a few integer literals
//
//  2. Print a few float literals
//
//  3. Print true and false bool literals
//
//  4. Print your name using a string literal
//
//  5. Print a non-english sentence using a string literal
//
// ---------------------------------------------------------

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	fmt.Println("Examples of supported data types in GO")

	fmt.Println(-200, -100, 0, 50, 100, " | Integer literals") // Integer literals

	fmt.Println(-50.5, -20.5, -0., 1., 100.56, "| Float literals") // Float literals

	fmt.Println(true, false, "| Boolean") // Boolean literals

	fmt.Println("", "A, a, B, b, C, c, are just string literals") // Empty string literals, A, b, C, c

	fmt.Println("お元気ですか = How are you in Japanise") // Unicode string literal

	// ---------------------------------------------------------
	// EXERCISE: Print hexes
	//
	//  1. Print 0 to 9 in hexadecimal
	//
	//  2. Print 10 to 15 in hexadecimal
	//
	//  3. Print 17 in hexadecimal
	//
	//  4. Print 25 in hexadecimal
	//
	//  5. Print 50 in hexadecimal
	//
	//  6. Print 100 in hexadecimal
	//
	// EXPECTED OUTPUT
	//  0 1 2 3 4 5 6 7 8 9
	//  10 11 12 13 14 15
	//  17
	//  25
	//  50
	//  100
	//
	// NOTES
	//  https://stackoverflow.com/questions/910309/how-to-turn-hexadecimal-into-decimal-using-brain
	//
	// https://simple.wikipedia.org/wiki/Hexadecimal_numeral_system
	//
	// ---------------------------------------------------------
	sep.Cust("Print integer, hexadecimal value")
	printHex()

}

func printHex() {

	fmt.Println(0, 1, 2, 3, 4, 5, 7, 8, 9, 11, 12, 13, 14, 15)
	sep.Cust("10 - 15 in hexadecimal")
	fmt.Printf("%x %x %x %x %x %x \n", 10, 11, 12, 13, 14, 15)
	sep.Cust("17 in hexadecimal")
	fmt.Printf("%d\n", 17)
	sep.Cust("25 in hexadecimal")
	fmt.Printf("%d\n", 25)
	sep.Cust("50 in hexadecimal")
	fmt.Printf("%d\n", 50)
	sep.Cust("100 in hexadecimal")
	fmt.Printf("%d\n", 100)

}
