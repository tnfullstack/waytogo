/*
	Comparision examples
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

// package level variable
var speed int = 100

func main() {

	fast := speed >= 80
	slow := speed < 20

	fmt.Printf("Faster speed type %T\n", fast)

	fmt.Printf("going fast? %t\n", fast)
	fmt.Printf("going slow? %t\n", slow)

	fmt.Printf("is it 100mph? %t\n", speed == 100)

	// Separator
	sep.Cust("Comparison & assignability")
	compare1()

}

func compare1() {

	speedA := 150.5        // Type Float64
	faster := speedA > 100 // 100 is typeless

	// speedA can compare to 100 directly because 100 is type less,
	// therefor compiler converted 100 to float64automatically
	fmt.Println("is the other car going faster?", faster)

	faster = speedA > float64(speed) // After type conversion, speedA and speed are both float64
	fmt.Println("is the other car going faster?", faster)

}
