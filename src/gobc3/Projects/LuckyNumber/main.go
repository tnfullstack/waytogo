package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	maxTurns = 5
	usage    = `
Welcome to the lucky number game!

The program will pick %d random numbers.
Your mission is to guess one of those numbers.

The greater your numbers is the harder it gests.

`
)

func main() {
	rand.Seed(time.Now().UnixNano())

	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Printf(usage, maxTurns)
		return
	}

	guess, err := strconv.Atoi(args[0])

	if err != nil {
		fmt.Println("Not a number.")
		return
	}

	if guess < 0 {
		fmt.Println("Please enter a positive number.")
		return
	}

	for turn := 0; turn < maxTurns; turn++ {
		n := rand.Intn(guess + 1)
		fmt.Println(n, guess)

		if n == guess {
			fmt.Printf("🏆 YOU WIN!, MATCH FOUND! %d %v\n", n, guess)
			return
		}
	}
	fmt.Println("☠️  YOU LOSS!, No match found!")
}
