package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Double Guesses
//
//  Let the player guess two numbers instead of one.
//
// HINT:
//  Generate random numbers using the greatest number
//  among the guessed numbers.
//
// EXAMPLES
//  go run main.go 5 6
//  Player wins if the random number is either 5 or 6.
// ---------------------------------------------------------

const (
	maxTurns = 5
	usage    = `
Welcome to the lucky number game!

The program will pick %d random numbers.
Your mission is to guess one of those numbers.
You have options to enter one or two guessing numbers.

The greater your numbers is the harder it gets.

Example: command [3] [5] enter

`
)

func main() {
	var (
		guess, guess1, guess2 int
		err1                  error
		err2                  error
	)
	// Set randome seed number
	rand.Seed(time.Now().UnixNano())

	// Read command-line arguments
	args := os.Args[1:]

	// Verify input arguments
	if len(args) < 1 {
		fmt.Printf(usage, maxTurns)
		return
	}

	// Convert input argument to Integer and check for error
	guess1, err1 = strconv.Atoi(args[0])
	guess2, err2 = strconv.Atoi(args[1])
	if err1 != nil || err2 != nil {
		fmt.Println("Not a number")
		return
	}

	// Check if the input numbers are positive number
	if guess1 < 0 || guess2 < 0 {
		fmt.Println("Please enter positive numbers.")
		return
	}

	// Generate random numbers using the greatest number from command-line argument
	for i := 0; i < maxTurns; i++ {
		if guess1 < guess2 {
			guess = guess2
		} else {
			guess = guess1
		}

		n := rand.Intn(guess + 1)
		fmt.Println(i, n, guess1, guess2)

		// Compare results and Print Output
		if n == guess1 || n == guess2 {
			fmt.Println("🏆 YOU WIN!")
			return
		}
	}
	fmt.Println("☠️  YOU LOSS!, try again?")
}
