package main

import "fmt"

func main() {
	var i int

loop:
	if i < 5 {
		fmt.Println("Loopping!")
		i++
		goto loop
	}
	fmt.Println("Done")
}
