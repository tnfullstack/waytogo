package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: First Turn Winner
//
//  If the player wins on the first turn, then display
//  a special bonus message.
//
// RESTRICTION
//  The first picked random number by the computer should
//  match the player's guess.
//
// EXAMPLE SCENARIO
//  1. The player guesses 6
//  2. The computer picks a random number and it happens
//     to be 6
//  3. Terminate the game and print the bonus message
// ---------------------------------------------------------

const (
	maxTurn = 5
	usage   = `
	Welcome to the Lucky Number Game!
	The program will pick %d random numbers.
	Your mission is to guess one of those numbers.
	
	The greater your numbers is, the harder it gets.
	
	Let play?
	`
)

func main() {

	// Set random seed number
	rand.Seed(time.Now().UnixNano())

	// Read command line argument
	args := os.Args[1:]

	// Test if there is an argument
	if len(args) != 1 {
		fmt.Printf(usage, maxTurn)
		return
	}

	// convert the input string to integer and assign to guess
	guess, err := strconv.Atoi(args[0])

	// check for convert err
	if err != nil {
		fmt.Println("Not a number.")
		return
		// Check if the input integet is a possitive number
	} else if guess < 0 {
		fmt.Println("Please enter a positive number")
		return
	}

	// generate random numbers
	for turn := 1; turn <= maxTurn; turn++ {
		n := rand.Intn(guess + 1)

		fmt.Println(turn, n)
		// Test if first guess match random number
		if turn == 1 && n == guess {
			fmt.Println("🏆 JACKPOT, YOU WIN at the first guess!")
			return
		}
		// Test if guess match random numbers
		if n == guess {
			fmt.Printf("👍 YOU WIN, at the guess number %d\n", turn)
			return
		}
	}
	// Print user loss message if no matching guess found
	fmt.Println("☠️  YOU LOSS... Try again?")
}
