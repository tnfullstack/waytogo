package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Enough Picks
//
//  If the player's guess number is below 10;
//  then make sure that the computer generates a random
//  number between 0 and 10.
//
//  However, if the guess number is above 10; then the
//  computer should generate the numbers
//  between 0 and the guess number.
//
// WHY?
//  This way the game will be harder.
//
//  Because, in the current version of the game, if
//  the player's guess number is for example 3; then the
//  computer picks a random number between 0 and 3.
//
//  So, currently a player can easily win the game.
//
// EXAMPLE
//  Suppose that the player runs the game like this:
//    go run main.go 9
//
//  Or like this:
//    go run main.go 2
//
//    Then the computer should pick a random number
//    between 0-10.
//
//  Or, if the player runs it like this:
//    go run main.go 15
//
//    Then the computer should pick a random number
//    between 0-15.
// ---------------------------------------------------------

const (
	maxTurns = 5
	usage    = `
Welcome to the lucky number game!

The program will pick %d random numbers.
Your mission is to guess one of those numbers.
You have options to enter display computer random numbers and your guessing numbers.

The greater your numbers is the harder it gets.

Example: command [-v] [5] enter

`
)

func main() {
	// Setup random seed numbers
	rand.Seed(time.Now().UnixNano())

	// Read command-line arguments
	args := os.Args[1:]

	// Test command-line arguments for proper entry data
	if len(args) < 1 {
		fmt.Printf(usage, maxTurns)
		return
	}

	// Convert command-line argument to integer
	guess, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Printf("%v is not a number.\n", args[0])
		return
	}

	if guess < 0 {
		fmt.Println("Please enter a positive number")
	}

	// Set mininum guess value
	min := 10
	if guess < min {
		guess = min
	}

	// Generate the random number base on guess number value
	for turn := 1; turn < maxTurns; turn++ {

		n := rand.Intn(guess + 1)
		fmt.Println(turn, n, guess)

		// Compare random and guess value and print results
		if n == guess {
			fmt.Println("🏆 YOU WIN!")
			return
		}
		fmt.Println("☠️  YOU LOSS!, try again?")
		return
	}
}
