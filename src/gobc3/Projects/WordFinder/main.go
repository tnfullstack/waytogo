package main

import (
	"fmt"
	"os"
	"strings"
)

const corpus = "" + "busy cat jumps again and again, and again"

func main() {

	words := strings.Fields(corpus)
	query := os.Args[1:]

queries:
	for _, q := range query {

		// Convert all words from query to lower case
		q = strings.ToLower(q)
	search:
		// Cycle through and matching each word in slice with query slice
		for j, w := range words {
			switch q {
			case "and", "or", "the":
				break search
			}
			if q == w {
				fmt.Printf("#%-2d: %q\n", j+1, w)
				continue queries
			}
		}
	}
}
