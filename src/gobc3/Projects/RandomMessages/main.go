package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Random Messages
//
//  Display a few different won and lost messages "randomly".
//
// HINTS
//  1. You can use a switch statement to do that.
//  2. Set its condition to the random number generator.
//  3. I would use a short switch.
//
// EXAMPLES
//  The Player wins: then randomly print one of these:
//
//  go run main.go 5
//    YOU WON
//  go run main.go 5
//    YOU'RE AWESOME
//
//  The Player loses: then randomly print one of these:
//
//  go run main.go 5
//    LOSER!
//  go run main.go 5
//    YOU LOST. TRY AGAIN?
// ---------------------------------------------------------

const (
	maxTurns = 5
	usage    = `
Welcome to the lucky number game!

The program will pick %d random numbers.
Your mission is to guess one of those numbers.

The greater your numbers is the harder it gets.

`
)

func main() {

	// Setup rand seed number
	rand.Seed(time.Now().UnixNano())

	// Read user input argument
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Printf(usage, maxTurns)
		return
	}
	// Convert input string to integer value
	guess, err := strconv.Atoi(args[0])

	// Test user input errors
	if err != nil {
		fmt.Println("Not a number")
	}

	if guess < 0 {
		fmt.Println("Please enter a positive number.")
	}

	// Generage random numbers base on game parameters
	for turn := 1; turn < maxTurns; turn++ {
		n := rand.Intn(guess + 1)
		fmt.Println(turn, n, guess)

		if n == guess {
			// Print result with random messages
			switch rand.Intn(maxTurns) {
			case 0:
				fmt.Println("🏆 YOU ARE THE BEST!")
			case 1:
				fmt.Println("🔆 You are awesome!")
			case 2:
				fmt.Println("👍 You are amazing!")
			case 3:
				fmt.Println("💪 You are unstopable!")
			default:
				fmt.Println("👍 You are great!")
			}
			return
		}
	}
	mes := "%s Try again?\n"

	switch rand.Intn(2) {
	case 0:
		fmt.Printf(mes, "☠️  YOU LOST...")
	case 1:
		fmt.Printf(mes, "☠️  JUST A BAD LUCK...")
	}
}
