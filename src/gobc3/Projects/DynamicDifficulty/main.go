package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Dynamic Difficulty
//
//  Current game picks only 5 numbers (5 turns).
//
//  Make sure that the game adjust its own difficulty
//  depending on the guess number.
//
// RESTRICTION
//  Do not make the game to easy. Only adjust the
//  difficulty if the guess is above 10.
//
// EXPECTED OUTPUT
//  Suppose that the player runs the game like this:
//    go run main.go 5
//
//    Then the computer should pick 5 random numbers.
//
//  Or, if the player runs it like this:
//    go run main.go 25
//
//    Then the computer may pick 11 random numbers
//    instead.
//
//  Or, if the player runs it like this:
//    go run main.go 100
//
//    Then the computer may pick 30 random numbers
//    instead.
//
//  As you can see, greater guess number causes the
//  game to increase the game turns, which in turn
//  adjust the game's difficulty dynamically.
//
//  Because, greater guess number makes it harder to win.
//  But, this way, game's difficulty will be dynamic.
//  It will adjust its own difficulty depending on the
//  guess number.
// ---------------------------------------------------------

const (
	usage = `
	Welcome to the lucky number game!

	The program will pick %d random numbers.
	Your mission is to guess one of those numbers.
	You have options to enter display computer random numbers and your guessing numbers.

	The greater your numbers is the harder it gets.

	Example: command [-v] [5] enter

`
)

func main() {
	maxTurns := 5

	// Set random seed number
	rand.Seed(time.Now().UnixNano())

	// Read command-line arguments
	args := os.Args[1:]

	// Validate command-line arguments
	if len(args) < 1 {
		fmt.Printf(usage, maxTurns)
		return
	}

	guess, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Not a number.")
		return
	}

	if guess < 0 {
		fmt.Println("Please enter a positive number.")
		return
	}

	// Set Dynamic maxturn number base on user input value
	if guess > 10 {
		maxTurns = maxTurns + guess/4
	}

	fmt.Printf("Maxturns %v\n", maxTurns)

	// Generate random numbers
	for i := 0; i < maxTurns; i++ {

		n := rand.Intn(guess + 1)

		fmt.Println(i, n, guess)

		// Compare random numbers and input value and print results
		if n == guess {
			fmt.Println("🏆 YOU WIN!")
			return
		}
	}
	fmt.Println("☠️  YOU LOSS!, Play again?")
}
