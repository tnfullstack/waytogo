// Slice Capacity Explain

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintBacking = true

	// Initial ages slice with len of 5 and capcity of 5
	ages := []int{19, 35, 43, 56, 93}
	s.Show("\nages:", ages)

	// Now let slice the ages to ages[0:3]
	ages = ages[0:3]
	s.Show("\nages[0:3]:", ages)

	// Slice to zero length ages[0:0]
	ages = ages[0:0]
	s.Show("\nages[0:0]", ages)

	// Restore the length or the slice
	ages = ages[0:5]
	s.Show("\nages[0:5]:", ages)

	// Now let see if we can increase the len/capacity of the slice
	// ages = args[0:6] // No, here we will see the compile time error

}
