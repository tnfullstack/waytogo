// Slice Header Example

package main

import (
	"fmt"

	s "github.com/inancgumus/prettyslice"
)

type collection []string

func main() {

	s.PrintElementAddr = true

	data := collection{"slice", "are", "awesome", "period"}

	change(data)
	s.Show("main's data", data)

	// Print the memory address of the data slice
	fmt.Printf("Main's data slice address: %p\n", &data)

}

func change(newData collection) {
	newData[2] = "brilliant!"
	s.Show("Change's data", newData)

	// Print the memory address of the newData slice
	fmt.Printf("Change's data slice address: %p\n", &newData)

}
