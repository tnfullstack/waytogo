// Slices Examples

package main

import "fmt"

func main() {

	// Array literal
	var books [5]string
	books[0] = "Draclula"
	books[1] = "1984"
	books[2] = "Island"

	newBooks := [5]string{"Ulysses", "Fire"}

	// books = newBooks
	fmt.Printf("books        : %#v\nnewBooks     : %#v\n", books, newBooks)

	fmt.Println("is books == newBooks? ", books == newBooks)

	// Slices literal
	games := []string{"Kokemon", "Sims"}
	newGames := []string{"packman", "doom", "pong"}

	games = newGames // these slices are hold the same elements

	var equal string

	for i := 0; i < len(games); i++ {
		if games[i] == newGames[i] {
			equal = "equal"
			break
		}
		equal = "not equal"
	}

	fmt.Println("games and newGames slices are ", equal)
	fmt.Println(games, newGames)

	fmt.Printf("books        : %#v\n", books)
	fmt.Printf("games        : %#v\n", games)

	fmt.Printf("games        : %T\n", games)
	fmt.Printf("games' len   : %d\n", len(games))
	fmt.Printf("nil?         : %t\n", games == nil)

}
