// Slice append function

package main

import "fmt"

func main() {

	var nums []int

	newNum := []int{51, 52, 53, 54, 55, 56, 57}

	nums = append(nums, 100)

	for i := 1; i <= 50; i++ {
		nums = append(nums, i)
	}

	for i := 0; i < len(nums); i++ {
		fmt.Print(nums[i], " ")
	}
	fmt.Println()

	nums = append(nums, newNum...)
	fmt.Println(nums)

}
