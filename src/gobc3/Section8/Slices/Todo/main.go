// Todo list example using slice and append

package main

import "fmt"

func main() {

	var todo []string

	todo = append(todo, "Read a book", "Cook dinner", "Walk the dog")

	tomorrow := []string{"Buy Milk", "Wash the Car", "Reading", "Exercise"}

	// Add tomorrow slice to todo slice
	todo = append(todo, tomorrow...)

	fmt.Println("My todo:")
	for i, v := range todo {
		fmt.Printf("#%d %q\n", i, v)
	}
}
