package main

import (
	"fmt"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Append #2
//
//  1. Create the following nil slices:
//     + Pizza toppings
//     + Departure times
//     + Student graduation years
//     + On/off states of lights in a room
//
//  2. Append them some elements (use your creativity!)
//
//  3. Print all the slices
//
//
// EXPECTED OUTPUT
// (Your output may change, mine is like so:)
//
//  pizza       : [pepperoni onions extra cheese]
//
//  graduations : [1998 2005 2018]
//
//  departures  : [2019-01-28 15:09:31.294594 +0300 +03 m=+0.000325020
//  2019-01-29 15:09:31.294594 +0300 +03 m=+86400.000325020
//  2019-01-30 15:09:31.294594 +0300 +03 m=+172800.000325020]
//
//  lights      : [true false true]
//
//
// HINTS
//  + For departure times, use the time.Time type. Check its documentation.
//
//      now := time.Now()     -> Gives you the current time
//      now.Add(time.Hour*24) -> Gives you a time.Time 24 hours after `now`
//
//  + For graduation years, you can use the int type
// ---------------------------------------------------------

func main() {

	var (
		pizza      []string
		departure  []time.Time
		graduation []int
		states     []bool
	)

	now := time.Now()
	in8hrs := now.Add(time.Hour * 8)
	in12hrs := now.Add(time.Hour * 12)

	pizza = append(pizza, "Pepperoni", "Supreme", "Hawaiian", "MMQ Meatlovers", "Garlic buuter prawns and chilli")
	departure = append(departure, now, in8hrs, in12hrs)
	graduation = append(graduation, 2020, 2016, 1986)
	states = append(states, true)

	fmt.Printf("Popular Pizza Toppings are: %s\n", pizza)
	fmt.Printf("Flight departure times are:\n %s\n %s\n %s\n", departure[0], departure[1], departure[2])
	fmt.Printf("Graduation year: %d\n", graduation)
	fmt.Printf("The light is on: %t\n", states)

}
