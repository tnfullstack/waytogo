package main

import (
	"fmt"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Compare the slices
//
//  1. Split the namesA string and get a slice
//
//  2. Sort all the slices
//
//  3. Compare whether the slices are equal or not
//
//
// EXPECTED OUTPUT
//
//   They are equal.
//
//
// HINTS
//
//   1. strings.Split function splits a string and
//      returns a string slice
//
//   2. Comparing slices: First check whether their length
//      are the same or not; only then compare them.
//
// ---------------------------------------------------------

func main() {
	namesA := "Da Vinci, Wozniak, Carmack"

	namesC := strings.Split(namesA, ", ")

	namesB := []string{"Wozniak", "Da Vinci", "Carmack"}

	fmt.Println("namesC and namesB are equal? ", len(namesC) == len(namesB))

	var ok string
	if len(namesC) == len(namesB) {

		for i, c := range namesC {

			if c == namesB[i] {
				ok = "is equal"
			}
			ok = "not equal"
		}
	}
	fmt.Printf("namesC is %v to namesB\n", ok)
	fmt.Println(namesC)
	fmt.Println(namesB)
}
