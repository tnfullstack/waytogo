// Slice Example with refactor of array to slices

package main

import (
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	max, _ := strconv.Atoi(os.Args[1])

	/*
			var uniques [10]int

		loop:
			for i := 0; i < max; {
				n := rand.Intn(max) + 1
				fmt.Print(n, " ")

				for _, u := range uniques {
					if u == n {
						continue loop
					}
				}

				uniques[i] = n
				i++
			}
			fmt.Println()
			fmt.Println("uniquies: ", uniques)
	*/
	// The refactor code block
	refactor(max)
}

func refactor(m int) {
	rand.Seed(time.Now().UnixNano())

	var uniques []int

loop:
	for len(uniques) < m {
		n := rand.Intn(m) + 1
		fmt.Print(n, " ")

		for _, u := range uniques {
			if u == n {
				continue loop
			}
		}

		uniques = append(uniques, n)

	}
	fmt.Println()
	fmt.Println("uniquies: ", uniques)

	sort.Ints(uniques)
	fmt.Println("sorted: ", uniques)

}
