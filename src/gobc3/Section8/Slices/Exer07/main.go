// Exercises Level II - Appending
// Append #1 - Append and compare byte slices

package main

import (
	"bytes"
	"fmt"
)

// ---------------------------------------------------------
// EXERCISE: Append
//
//  Please follow the instructions within the code below.
//
// EXPECTED OUTPUT
//  They are equal.
//
// HINTS
//  bytes.Equal function allows you to compare two byte
//  slices easily. Check its documentation: go doc bytes.Equal
// ---------------------------------------------------------

func main() {
	// 1. uncomment the code below
	png, header := []byte{'P', 'N', 'G'}, []byte{}

	// 2. append elements to header to make it equal with the png slice
	header = append(header, png...)

	// 3. compare the slices using the bytes.Equal function
	if bytes.Equal([]byte(png), []byte(header)) {
		// 4. print whether they're equal or not
		fmt.Println("Yes, png and header are equal")
	} else {
		fmt.Println("No png and header are not equal!")
	}

	// Show the two slices
	fmt.Println(png, " == ", header)
}
