// Append Exercies
// Exercices Level 2 - Appending

package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Append #3 — Fix the problems
//
//  Fix the problems in the code below.
//
// BONUS
//
//  Simplify the code.
//
// EXPECTED OUTPUT
//
//  toppings: [black olives green peppers onions extra cheese]
//
// ---------------------------------------------------------

func main() {
	toppings := []string{"black olives", "green peppers"}

	pizza := []string{"onions", "extra cheese"}
	toppings = append(toppings, pizza...)

	fmt.Printf("toppings       : %s\n", toppings)
}
