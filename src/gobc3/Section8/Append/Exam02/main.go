// Todo list Example

package main

import s "github.com/inancgumus/prettyslice"

func main() {

	var todo []string
	/*
		// Append some more elements to the todo slice
		todo = append(todo, "Groceries Shopping")
		todo = append(todo, "House Cleaning")
		todo = append(todo, "Car Wash")
		todo = append(todo, "Pay bills")
	*/

	// A different way to append elements to a slice
	todo = append(todo, "Groceries", "House Cleaning", "Car Wash", "Pay bills")

	// We can also add a todo list to another todo list
	tomorrow := []string{"Tennis", "Diner"}
	todo = append(todo, tomorrow...)

	s.Show("todo", todo)

}
