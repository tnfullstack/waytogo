// Append Exercies
// Exercices Level 2 - Appending

package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Append and Sort Numbers
//
//  We'll have a []string that should contain numbers.
//
//  Your task is to convert the []string to an int slice.
//
//  1. Get the numbers from the command-line
//
//  2. Append them to an []int
//
//  3. Sort the numbers
//
//  4. Print them
//
//  5. Handle the error cases
//
//
// EXPECTED OUTPUT
//
//  go run main.go
//    provide a few numbers
//
//  go run main.go 4 6 1 3 0 9 2
//    [0 1 2 3 4 6 9]
//
//  go run main.go a b c
//    []
//
//  go run main.go 4 a 1 c d 9
//    [1 4 9]
//
// ---------------------------------------------------------

func main() {

	// Read argument from command-line
	args := os.Args[1:]

	// validate if there is any argument exist
	if len(args) < 1 {
		fmt.Println("Usage: command [3 4 5 8 9...]")
		return
	}

	// slice for storing numbers
	var nums []int

	// iterate through arguments and convert each element in integer
loop:
	for _, n := range args {
		temp, err := strconv.Atoi(n)

		// inform user if there is error in argument list
		if err != nil {
			continue loop
		}

		// store the numbers to nums slice
		nums = append(nums, temp)
	}

	// Sort the nums slice, then print out result
	sort.Ints(nums)
	fmt.Printf("sorted nums: %v\n", nums)

}
