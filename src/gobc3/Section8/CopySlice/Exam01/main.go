// copy() : Copy elements between sllices

package main

import "fmt"

func main() {
	data := []float64{10, 25, 38, 49, 53}

	fmt.Printf("data: %v\n", data)

	// Copy will over write date from receiving array
	// the number of over wrote elements is depend on the number of new elements
	copy(data, []float64{99, 100})
	fmt.Printf("data: %.f\n", data)

	// New data to be copying
	// data slice has 5 elements
	// but there are 6 elements to be copied, in this case the last element will no be copied.
	// n := copy(data, []float64{15, 20, 35, 45, 55, 65})

	// in order to copy all elements we need to refactor the code
	// but this is not the copy function.
	// data = append(data[:0], []float64{15, 20, 35, 45, 55, 65}...)
	// fmt.Printf("data: %.f\n", data)

	// Let refactor with the copy function
	// saved := make([]float64, len(data)) // This only creat and empty slice with the len(data)
	// Now let copy content of data into saved
	// copy(saved, data)

	// copying useing append is however a better idea
	saved := append([]float64(nil), data...)

	// Now changing saved won't effect data slice
	saved = append(saved, 87, 97, 88)
	fmt.Printf("saved: %.f\n", saved)
	fmt.Printf("data %.f\n", data)

	var total float64

	for _, v := range data {
		total += v
	}
	fmt.Printf("total : %.f\n", total)
	average := total / float64(len(data))
	fmt.Printf("The everage properability of rain this week is %.f%% chance.\n", average)

}
