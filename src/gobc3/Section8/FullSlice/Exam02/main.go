// Full Slice Expression: Limit the capacity of the slice
// Exmaple 2

package main

import s "github.com/inancgumus/prettyslice"

func main() {

	s.PrintBacking = true

	// Creat a slice []int
	score := []int{34, 56, 87, 39, 23}

	// Slice off 0 to 3
	score = score[0:3:5]
	// Print out the new slice
	s.Show("score:", score)

	// Swrink the slice capacity
	score = score[0:3:4]
	s.Show("score[0:3:4", score)

	// Return the to full capacity
	score = score[0:4:4]
	s.Show("score[0:4:4", score)

	score = append(score, 56, 43, 89, 45, 34)
	s.Show("score[0:5:8", score)

}
