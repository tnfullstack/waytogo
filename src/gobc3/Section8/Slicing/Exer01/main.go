// Slicing Exercises Level III
// Discover the power of slicing

package main

import (
	"fmt"
	"strconv"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Slice the numbers
//
//   We've a string that contains even and odd numbers.
//
//   1. Convert the string to an []int
//
//   2. Print the slice
//
//   3. Slice it for the even numbers and print it (assign it to a new slice variable)
//
//   4. Slice it for the odd numbers and print it (assign it to a new slice variable)
//
//   5. Slice it for the two numbers at the middle
//
//   6. Slice it for the first two numbers
//
//   7. Slice it for the last two numbers (use the len function)
//
//   8. Slice the evens slice for the last one number
//
//   9. Slice the odds slice for the last two numbers
//
//
// EXPECTED OUTPUT
//   go run main.go
//
//     nums        : [2 4 6 1 3 5]
//     evens       : [2 4 6]
//     odds        : [1 3 5]
//     middle      : [6 1]
//     first 2     : [2 4]
//     last 2      : [3 5]
//     evens last 1: [6]
//     odds last 2 : [3 5]
//
//
// NOTE
//
//  You can also use my prettyslice package for printing the slices.
//
//
// HINT
//
//  Find a function in the strings package for splitting the string into []string
//
// ---------------------------------------------------------

func main() {
	// uncomment the declaration below
	data := "2 4 6 1 3 5 A B C"

	// Convert the data string string to []string
	sliceStr := strings.Split(data, " ")

	var nums []int
	// Convert []string to []int
loop:
	for _, el := range sliceStr {
		num, err := strconv.Atoi(el)
		if err != nil {
			fmt.Println(err)
			continue loop
		}
		// store num in []nums
		nums = append(nums, num)
	}

	// Print out the []int
	fmt.Printf("Nums slice: %T %d\n", nums, nums)

	// Slice the []nums into even number only
	even := nums[:3]
	fmt.Printf("even slice: %d\n", even)

	// Slice out the odd num from []nums
	odd := nums[3:]
	fmt.Printf("odd slice: %d\n", odd)

	// Slice out the two number in the middle of []nums
	mid := nums[2:4]
	fmt.Printf("mid slice: %d\n", mid)

	// Slice out the last two number from []nums
	l2 := nums[4:len(nums)]
	fmt.Printf("last 2 num: %d\n", l2)

	// Slice out the last number from []even
	ln := even[len(even)-1:]
	fmt.Printf("Last number: %d\n", ln)

	// Slice out the last two number from []odd
	ltwo := odd[len(odd)-2:]
	fmt.Printf("last two num from odd %d\n", ltwo)
}
