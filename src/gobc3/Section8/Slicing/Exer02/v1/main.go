// Slicing Exercises Level III
// Discover the power of slicing

package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Slicing by arguments
//
//   We've a []string, you will get arguments from the command-line,
//   then you will print only the elements that are requested.
//
//   1. Print the []string (it's in the code below)
//
//   2. Get the starting and stopping positions from the command-line
//
//   3. Print the []string slice by slicing it using the starting and stopping
//      positions
//
//   4. Handle the error cases (see the expected output below)
//
//   5. Add new elements to the []string slice literal.
//      Your program should work without changing the rest of the code.
//
//   6. Now, play with your program, get a deeper sense of how the slicing
//      works.
//
//
// EXPECTED OUTPUT
//
//  go run main.go
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Provide only the [starting] and [stopping] positions
//
//
//  (error because: we expect only two arguments)
//
//  go run main.go 1 2 4
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Provide only the [starting] and [stopping] positions
//
//
//  (error because: starting index >= 0 && stopping pos <= len(slice) )
//
//  go run main.go -1 5
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Wrong positions
//
//
//  (error because: starting <= stopping)
//
//  go run main.go 3 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Wrong positions
//
//
//  go run main.go 0
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Normandy Verrikan Nexus Warsaw]
//
//
//  go run main.go 1
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan Nexus Warsaw]
//
//
//  go run main.go 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Nexus Warsaw]
//
//
//  go run main.go 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Warsaw]
//
//
//  go run main.go 4
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    []
//
//
//  go run main.go 0 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Normandy Verrikan Nexus]
//
//
//  go run main.go 1 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan Nexus]
//
//
//  go run main.go 1 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan]
//
// ---------------------------------------------------------

func main() {

	// uncomment the slice below
	ships := []string{"HMM Algeciras", "HMM Oslo", "MSC Gulsun", "MSC Mina",
		"OOC Hong Kong", "COSCO Universe", "Madrid Marsk", "Ever Golden",
		"MOL Truth", "MOL Triumph", "Ever Glory", "Ever Goods",
		"COSCO Taurus", "Barzan", "Panamax", "Feedermax",
	}

	var (
		selected   []string
		start, end = 0, len(ships)
		err1, err2 error
	)

	// Read data from command-line
	args := os.Args[1:]

	if len(args) < 1 || len(args) > 2 {
		fmt.Println("\nUsage: command [0 5]")
		fmt.Println("This will print ship name from 0 to 5")
		fmt.Println("Here is a list for your reference" + "\n")
		for i, s := range ships {
			fmt.Printf("#%d  : %s\n", i, s)
		}
		fmt.Println()
		return
	}

	if len(args) == 1 {
		start, err1 = strconv.Atoi(args[0])
		end = len(ships)
	}

	if len(args) == 2 {
		end, err2 = strconv.Atoi(args[1])
	}

	if err1 != nil || err2 != nil {
		fmt.Println("\nStart and End value must be positive numbers." + "\n")
		return
	}

	if start < 0 || end < 0 {
		fmt.Println("\nSorry we can not process negative value." + "\n")
		return
	}

	if start >= end {
		fmt.Println("\nStart value must be smaller than end value." + "\n")
		return
	}

	if end > len(ships) {
		fmt.Printf("\nStart and End must be between %d and %d\n\n", start-start, len(ships))
		return
	}

	selected = ships[start:end]

	fmt.Println("\nHere is your selected list:" + "\n")
	for i, el := range selected {
		fmt.Printf("#%d\t %s\n", i+1, el)
	}

	fmt.Println()
}
