// Slicing Exercises Level III
// Discover the power of slicing

package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Slicing by arguments
//
//   We've a []string, you will get arguments from the command-line,
//   then you will print only the elements that are requested.
//
//   1. Print the []string (it's in the code below)
//
//   2. Get the starting and stopping positions from the command-line
//
//   3. Print the []string slice by slicing it using the starting and stopping
//      positions
//
//   4. Handle the error cases (see the expected output below)
//
//   5. Add new elements to the []string slice literal.
//      Your program should work without changing the rest of the code.
//
//   6. Now, play with your program, get a deeper sense of how the slicing
//      works.
//
//
// EXPECTED OUTPUT
//
//  go run main.go
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Provide only the [starting] and [stopping] positions
//
//
//  (error because: we expect only two arguments)
//
//  go run main.go 1 2 4
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Provide only the [starting] and [stopping] positions
//
//
//  (error because: starting index >= 0 && stopping pos <= len(slice) )
//
//  go run main.go -1 5
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Wrong positions
//
//
//  (error because: starting <= stopping)
//
//  go run main.go 3 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    Wrong positions
//
//
//  go run main.go 0
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Normandy Verrikan Nexus Warsaw]
//
//
//  go run main.go 1
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan Nexus Warsaw]
//
//
//  go run main.go 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Nexus Warsaw]
//
//
//  go run main.go 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Warsaw]
//
//
//  go run main.go 4
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    []
//
//
//  go run main.go 0 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Normandy Verrikan Nexus]
//
//
//  go run main.go 1 3
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan Nexus]
//
//
//  go run main.go 1 2
//    ["Normandy" "Verrikan" "Nexus" "Warsaw"]
//
//    [Verrikan]
//
// ---------------------------------------------------------

const usage = `Usage: command [0 5]
First number must be smaller the second number.
Here are some example:
command 0 (print name from 1fst to end of the list)
command 0 5 (print from 1fst to 5th)
command 2 (print from 2nd to end of the list)
`

func main() {

	// uncomment the slice below
	ships := []string{"HMM Algeciras", "HMM Oslo", "MSC Gulsun", "MSC Mina",
		"OOC Hong Kong", "COSCO Universe", "Madrid Marsk", "Ever Golden",
		"MOL Truth", "MOL Triumph", "Ever Glory", "Ever Goods",
		"COSCO Taurus", "Barzan", "Panamax", "Feedermax",
	}

	// Read data from command-line
	args := os.Args[1:]

	var (
		start, end int
		err1, err2 error
	)

	// Start validating command-line arguments block
	switch len(args) {

	// If no number was enter, print the usage instruction
	case 0:
		fmt.Println("\n" + usage)

		// If user enter only on number do case 1
	case 1:
		start, err1 = strconv.Atoi(args[0])
		end = len(ships)
		if err1 != nil {
			fmt.Println("\nPlease enter a valid number" + "\n")
			return
		}

		// If use enter start and end numbers do case 2
	case 2:
		start, err1 = strconv.Atoi(args[0])
		end, err2 = strconv.Atoi(args[1])
		if err1 != nil || err2 != nil {
			fmt.Printf("\nPlease enter [start] [end] numbers from %d to %d\n\n", start, len(ships))
			return
		}
		// If user enter more than two numbers print default message
	default:
		fmt.Println("\nPlease enter valid only [start] [end] value" + "\n")
		return
	}
	// Check the final end tries for testing purpose
	fmt.Println("This just ofor testing purpose: ", start, end)

	// validate start is not greater than end
	if start >= end {
		fmt.Println("\n" + usage)
		return
	}

	// slice the selected number from []ships
	selected := ships[start:end]

	// Print out the list or ships from []selected
	fmt.Println("\nHere is you selected list:" + "\n")
	for i, n := range selected {
		fmt.Printf("#%d\t:%s\n", i+1, n)
	}
	fmt.Println()
}
