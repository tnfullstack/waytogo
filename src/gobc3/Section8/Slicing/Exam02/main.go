// Create pagination using slice (+ Sprintf)

package main

import (
	"fmt"

	s "github.com/inancgumus/prettyslice"
)

func main() {

	items := []string{
		"packman", "mario", "tetris", "doom",
		"galaga", "frogger", "astreroids", "simcity",
		"metroid", "defender", "rayman", "tempest",
		"ultima",
	}

	// Print out 4 elements per page
	/*
		s.Show("0:4 %q\n", items[0:4])
		s.Show("4:8 %s\n", items[4:8])
		s.Show("8:12 %s\n", items[8:12])
		s.Show("12:14 %s\n", items[12:])
	*/

	// A better way to do pagination
	const pageSize = 4

	l := len(items)

	for from := 0; from < l; from += pageSize {
		to := from + pageSize

		if to > l {
			to = l
		}

		// fmt.Printf("%d:%d\n", from, to)
		currentPage := items[from:to]
		head := fmt.Sprintf("Page #%d", (from/pageSize)+1)
		s.Show(head, currentPage)
	}

}
