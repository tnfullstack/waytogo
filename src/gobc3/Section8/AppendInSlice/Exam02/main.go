// When does the append function create a new backing array?

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintElementAddr = true
	s.MaxPerLine = 8

	// Let create a nil slice and print it out
	var nums []int
	s.Show("No backing array", nums)

	// Now let append some elments to nums slice
	nums = append(nums, 1, 3)
	s.Show("nums", nums)

	// Now let append some more elements to nums
	nums = append(nums, 4, 5, 8)
	s.Show("nums", nums)

	// Refactor this block of code from Exam01
	temp := nums[2:] // Not working
	nums = nums[2:]
	s.Show("temp", temp)
	s.Show("nums", nums)

	// When append new alements, append function always put the new elements to the end of the slice
	// Now let try to append some elements in the middle of the slice.
	nums = append(nums[:2], 1, 1)
	// nums = nums[:9]
	s.Show("After added 1, 1 starting at index #2:", nums)

	// Now add temp back to end of the slice
	nums = append(nums, temp...)
	s.Show("Check nums slice again, nums)
}
