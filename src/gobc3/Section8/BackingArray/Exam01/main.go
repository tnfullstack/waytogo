// Slice internal | Backing array example 1

package main

import "fmt"

func main() {

	nums := []int{23, 34, 56, 43, 54}
	num1 := nums

	fmt.Printf("nums : %#v\n", nums)
	fmt.Printf("num1 : %#v\n", num1)

	// num1[0] = 100
	// fmt.Printf("nums : %#v\n", nums)
	// fmt.Printf("num1 : %#v\n", num1)

	num2 := nums[0:2]
	num2[0] = 200
	num3 := nums[2:]
	num3[0] = 300
	fmt.Printf("nums : %#v\n", nums)
	fmt.Printf("num2 : %#v\n", num2)
	fmt.Printf("num3 : %#v\n", num3)

}
