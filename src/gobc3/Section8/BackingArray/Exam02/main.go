// Slice internal | Backing array example 2

package main

import (
	"fmt"
	"sort"
)

func main() {

	grades := [...]float64{40, 12, 34, 45, 56, 98}
	fmt.Println(grades)

	// front slice is not a new copy of the grades, it is just a link or pointer to grades array
	front := grades[:]
	front[1] = 200

	// The output of []front and [6]grades arrays are the same
	fmt.Println(front)
	fmt.Println(grades)

	// sort front will also sort grades
	sort.Float64s(front)
	fmt.Println(grades)

	// Now let make the acctual copy of the array
	grades1 := grades
	if grades1 == grades1 {
		fmt.Println(grades)
		fmt.Println(grades1)
	}

	// Now let change the element value of grades1 array
	grades1[1] = 500
	fmt.Println(grades1)
	fmt.Println(grades)

	// Let see if we can sort one of the array with sort.Float64s()
	// sort.Float64s(grades1)	// compile error, because sort.Floats64() only work with slice

	nums := []int{9, 7, 5, 3, 1}
	nums = nums[:1]
	fmt.Println(nums)

}
