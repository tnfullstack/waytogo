// How to loop over a slise

package main

import (
	"fmt"
	"gobc3/package/sep"
	"os"
	"strings"
)

func main() {
	arg := os.Args
	sep.Cust("Loop through input slice")
	for i := 1; i < len(arg); i++ {
		fmt.Printf("%q\n", arg[i])
	}
	sep.Cust("Loop throug a slice string")
	sliceString()
}

func sliceString() {

	str := strings.Fields("Thiên Chúa Là Tình Yêu Và Lẽ Sống Của Đời Con")

	for i := 0; i < len(str); i++ {
		fmt.Printf("#%-3d: %q\n", i+1, str[i])
	}

}
