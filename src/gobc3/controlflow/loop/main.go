// Loop Examples

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	var sum int
	// Traditional for loop
	sep.Cust("Traditional for loop")
	for i := 1; i < 10; i++ {
		sum += i
		fmt.Println(i, "-->", sum)
	}
	fmt.Println(sum)

	sep.Cust("for loop with a single condition")
	forLoop1()

	sep.Cust("infinit loop with break")
	forLoop2()

	sep.Cust("for loop with continue")
	forLoop3()
}

// for statement with single statement
func forLoop1() {

	var (
		sum int
		i   = 1
	)

	for i < 10 {
		sum += i
		i++
		fmt.Println(i, "-->", sum)
	}
	fmt.Println(i, sum)
}

// infinit for statement with break
func forLoop2() {

	var (
		sum int
		i   int
	)

	for {

		if i >= 10 {
			break
		}
		sum += i
		i++
		fmt.Println(i, "-->", sum)
	}
	fmt.Println(i, sum)
}

// for loop with continue
func forLoop3() {

	var (
		sum int
		i   = 1
	)

	for {
		if i > 10 {
			break
		}

		if i%2 != 0 {
			i++
			continue
		}

		sum += i
		fmt.Println(i, "-->", sum)
		i++
	}
}
