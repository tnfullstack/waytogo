// For Range: Learn looping truough slice the easy way

package main

import (
	"fmt"
	"strings"
)

func main() {

	str := strings.Fields("Thiên Chúa Là Tình Yêu Và Lẽ Sống Của Đời Con")

	/*
		// Code from previous example
		for i := 1; i < len(os.Args); i++ {
			fmt.Printf("%q\n", os.Args[i])
		}
	*/

	// Refactor into for range code example
	var (
		i int
		v string
	)
	for i, v = range str {
		fmt.Printf("%-2d: %q\n", i+1, v)
	}
	fmt.Printf("Last value of i = %d, q = %q\n", i, v)
}
