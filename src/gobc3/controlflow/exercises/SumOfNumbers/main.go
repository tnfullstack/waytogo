package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Sum the Numbers
//
//  1. By using a loop, sum the numbers between 1 and 10.
//  2. Print the sum.
//
// EXPECTED OUTPUT
//  Sum: 55
// ---------------------------------------------------------

func main() {
	var num int
	for i := 1; i <= 10; i++ {
		num += i
		fmt.Println(i, num)
	}
	fmt.Println(num)
}
