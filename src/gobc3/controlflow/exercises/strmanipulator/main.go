package main

import (
	"fmt"
	"os"
	"strings"
)

// ---------------------------------------------------------
// STORY
//  You want to write a program that will manipulate a
//  given string to uppercase, lowercase, and title case.
//
// EXERCISE: String Manipulator
//
//  1. Get the operation as the first argument.
//
//  2. Get the string to be manipulated as the 2nd argument.
//
// HINT
//  You can find the manipulation functions in the strings
//  package Go documentation (ToLower, ToUpper, Title).
//
// EXPECTED OUTPUT
//
//  go run main.go
//    [command] [string]
//
//    Available commands: lower, upper and title
//
//  go run main.go lower 'OMG!'
//    omg!
//
//  go run main.go upper 'omg!'
//    OMG!
//
//  go run main.go title "mr. charles darwin"
//    Mr. Charles Darwin
//
//  go run main.go genius "mr. charles darwin"
//    Unknown command: "genius"
// ---------------------------------------------------------
const (
	usage = `
usage:
$ command [option] [string]
Avalable options: lower, upper, and title
Example: command [lower] [John]
`
	unknown = `
I can only take 4 arguments at this time, sorry!
`
)

func main() {

	var (
		o, s string
	)
	// Get arguments from command line
	arg := os.Args

	// Verify armuments errors
	if len(arg) < 3 {
		fmt.Println(usage)
		return
	} else if len(arg) > 5 {
		fmt.Println(unknown)
		return
	}

	// Record opertor option
	o = arg[1]

	// Create final string base on inputs
	// Then send to final string to strManipulation function
	if len(arg) == 5 {
		s = arg[2] + " " + arg[3] + " " + arg[4] // create final string
		strManipulation(o, s)                    // Call string manipulation function
	} else if len(arg) == 4 {
		s = arg[2] + " " + arg[3]
		fmt.Println(o, s)
		strManipulation(o, s)
	} else {
		s = arg[2]
		fmt.Println(o, s)
		strManipulation(o, s)
	}
}

func strManipulation(o, s string) {
	var (
		op  = o    // record function signature
		str string // declare holder variable for function signature
	)
	switch { // Decide which operator option to exist
	case op == "upper": // check for operator option
		str = strings.ToUpper(s) // copy converted string to str variable
	case op == "lower":
		str = strings.ToLower(s)
	case op == "title":
		str = strings.Title(s)
	}
	fmt.Println(op, str)
}
