// Exercise: create a greeting programbase on time of the day

package main

import (
	"fmt"
	"time"
)

func main() {

	// Get time of the day
	// arg := os.Args
	// t, _ := strconv.Atoi(arg[1])
	t := time.Now().Hour()

	fmt.Printf("The time is %v:00hr\n", t)

	// Determind what time of the day is and out put a greeting
	if t >= 6 && t <= 11 {
		fmt.Println("Good morning!")
	} else if t >= 12 && t <= 17 {
		fmt.Println("Good afternoon!")
	} else if t >= 18 && t <= 23 {
		fmt.Println("Good evening!")
	} else {
		fmt.Println("Good night!")
	}

	// using switch statement
	switch {
	case t >= 18:
		fmt.Println("Good evening!")
	case t >= 11:
		fmt.Println("Good afternoon!")
	case t >= 6:
		fmt.Println("Good morning!")
	default:
		fmt.Println("Good night!")
	}
}
