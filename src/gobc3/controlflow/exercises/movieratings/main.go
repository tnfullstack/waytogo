package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// STORY
//
//  Your boss wants you to create a program that will check
//  whether a person can watch a particular movie or not.
//
//  Imagine that another program provides the age to your
//  program. Depending on what you return, the other program
//  will issue the tickets to the person automatically.
//
// EXERCISE: Movie Ratings
//
//  1. Get the age from the command-line.
//
//  2. Return one of the following messages if the age is:
//     -> Above 17         : "R-Rated"
//     -> Between 13 and 17: "PG-13"
//     -> Below 13         : "PG-Rated"
//
// RESTRICTIONS:
//  1. If age data is wrong or absent let the user know.
//  2. Do not accept negative age.
//
// BONUS:
//  1. BONUS: Use if statements only twice throughout your program.
//  2. BONUS: Use an if statement only once.
//
// EXPECTED OUTPUT
//  go run main.go 18
//    R-Rated
//
//  go run main.go 17
//    PG-13
//
//  go run main.go 12
//    PG-Rated
//
//  go run main.go
//    Requires age
//
//  go run main.go -5
//    Wrong age: "-5"
// ---------------------------------------------------------
const (
	noinput = `
usage: command [age] 			Enter age in positive integer.
Example: command 18
`
	negAge    = "Wrong age:"
	ratedR    = "R-Rated"
	ratedPG13 = "PG-13"
	ratedPG   = "PG-Rated"
	errnum    = "is not a number"
)

func main() {

	// Get age input from command line
	if age := os.Args; len(age) != 2 {
		fmt.Println(noinput)
	} else if n, err := strconv.Atoi(age[1]); err != nil {
		fmt.Println(age[1], errnum)
	} else if n < 0 {
		fmt.Println(negAge, n)
	} else if n > 17 {
		fmt.Println(ratedR)
	} else if n >= 13 && n <= 17 {
		fmt.Println(ratedPG13)
	} else {
		fmt.Println(ratedPG)
	}

	// 1. input data validation and let user know if data is wrong
	// 2. Do not accept negative age.

	// Use if statement only twice throughout the program
	// Use an if statement only once
	// If age > 17 => Return message "R-Rated"
	// Expected out put: R-Rated

	// if age >= 13 and age <= 17 - return message "PG-13"
	// Expected out put: PG-13

	// if age < 13 return message - "PG-Rated"
	// Expected output: PG-Rated

	// if no input return message
	// Expected output: Requires age

	// if input is negative -5
	// Expected output: Wrong age: "-5"
}
