package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Days in a Month
//
//  Print the number of days in a given month.
//
// RESTRICTIONS
//  1. On a leap year, february should print 29. Otherwise, 28.
//
//     Set your computer clock to 2020 to see whether it works.
//
//  2. It should work case-insensitive. See below.
//
//     Search on Google: golang pkg strings ToLower
//
//  3. Get the current year using the time.Now()
//
//     Search on Google: golang pkg time now year
//
//
// EXPECTED OUTPUT
//
//  -----------------------------------------
//  Your solution should not accept invalid months
//  -----------------------------------------
//  go run main.go
//    Give me a month name
//
//  go run main.go sheep
//    "sheep" is not a month.
//
//  -----------------------------------------
//  Your solution should handle the leap years
//  -----------------------------------------
//  go run main.go january
//    "january" has 31 days.
//
//  go run main.go february
//    "february" has 28 days.
//
//  go run main.go march
//    "march" has 31 days.
//
//  go run main.go april
//    "april" has 30 days.
//
//  go run main.go may
//    "may" has 31 days.
//
//  go run main.go june
//    "june" has 30 days.
//
//  go run main.go july
//    "july" has 31 days.
//
//  go run main.go august
//    "august" has 31 days.
//
//  go run main.go september
//    "september" has 30 days.
//
//  go run main.go october
//    "october" has 31 days.
//
//  go run main.go november
//    "november" has 30 days.
//
//  go run main.go december
//    "december" has 31 days.
//
//  -----------------------------------------
//  Your solution should be case insensitive
//  -----------------------------------------
//  go run main.go DECEMBER
//    "DECEMBER" has 31 days.
//
//  go run main.go dEcEmBeR
//    "dEcEmBeR" has 31 days.
// ---------------------------------------------------------
const (
	noinput = `
usage: command [month] [year]	Enter month and year as Jan or January 2016.
Example: command [[Jan][January][1]] [2016]
`
	yErr = "is not a number"
	mErr = "is not a valid month"
)

var (
	lyear bool
	y     int
	m     string
	err   error
)

func main() {

	day := 28

	arg := os.Args

	// Convert correct input in lower case
	m = strings.ToLower(arg[1])

	// Get the month input value from 1 - 12 only
	// Check input for error and let user know
	if len(arg) != 3 {
		fmt.Println(noinput)
		return
	} else if y, err = strconv.Atoi(arg[2]); err != nil {
		fmt.Println(arg[2], yErr)
		return
	}

	lyear = leapYear(y)

	// Check for current the month and year to determind days in month
	if (m == "february" || m == "feb" || m == "2") && lyear {
		day++
		printDaysInMonth(m, y, day)
	} else if (m == "february" || m == "feb" || m == "2") && !lyear {
		printDaysInMonth(m, y, day)
	} else if m == "jan" || m == "january" || m == "1" ||
		m == "mar" || m == "march" || m == "3" ||
		m == "may" || m == "5" ||
		m == "jul" || m == "july" || m == "7" ||
		m == "aug" || m == "august" || m == "8" ||
		m == "oct" || m == "october" || m == "10" ||
		m == "dec" || m == "december" || m == "12" {
		day += 3
		printDaysInMonth(m, y, day)
	} else if m == "apr" || m == "april" || m == "4" ||
		m == "jun" || m == "june" || m == "6" ||
		m == "sep" || m == "september" || m == "9" ||
		m == "nov" || m == "november" || m == "11" {
		day += 2
		printDaysInMonth(m, y, day)
	} else {
		fmt.Println("Some thing went wrong, please enter the correct month and year")
	}
}

func leapYear(l int) bool {
	if l%4 == 0 && (l%100 != 0 || l%400 == 0) {
		lyear = true
	} else {
		lyear = false
	}
	return lyear
}

func printDaysInMonth(m string, y, d int) {
	if m == "1" {
		m = "January"
	} else if m == "2" {
		m = "February"
	} else if m == "3" {
		m = "March"
	} else if m == "4" {
		m = "April"
	} else if m == "5" {
		m = "May"
	} else if m == "6" {
		m = "June"
	} else if m == "7" {
		m = "July"
	} else if m == "8" {
		m = "August"
	} else if m == "9" {
		m = "September"
	} else if m == "10" {
		m = "October"
	} else if m == "11" {
		m = "November"
	} else if m == "12" {
		m = "December"
	}
	fmt.Printf("%q of %v has %v days\n", m, y, d)
}
