package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Days in a Month
//
//  Refactor the previous exercise from the if statement
//  section.
//
//  "Print the number of days in a given month."
//
//  Use a switch statement instead of if statements.
// ---------------------------------------------------------

const (
	noinput = `
usage: command [month] [year]	Enter month and year as Jan or January 2016.
Example: command [[Jan][January][1]] [2016]
`
	yErr = "is not a number"
	mErr = "is not a valid month"
)

func main() {

	var (
		y   int
		err error
	)

	day := 28
	arg := os.Args

	// Convert correct input in lower case
	m := strings.ToLower(arg[1])

	// Get the month input value from 1 - 12 only
	// Check input for error and let user know
	if len(arg) != 3 {
		fmt.Println(noinput)
		return
	} else if y, err = strconv.Atoi(arg[2]); err != nil {
		fmt.Println(arg[2], yErr)
		return
	}

	lyear := leapYear(y)

	// Check for current the month and year to determind days in month
	switch {
	case (m == "february" || m == "feb" || m == "2") && lyear:
		m = "February"
		day++
	case (m == "february" || m == "feb" || m == "2") && !lyear:
		m = "February"
	case m == "jan" || m == "january" || m == "1":
		m = "January"
		day += 3
	case m == "mar" || m == "march" || m == "3":
		m = "March"
		day += 3
	case m == "may" || m == "5":
		m = "May"
		day += 3
	case m == "jul" || m == "july" || m == "7":
		m = "July"
		day += 3
	case m == "aug" || m == "august" || m == "8":
		m = "August"
		day += 3
	case m == "oct" || m == "october" || m == "10":
		m = "October"
		day += 3
	case m == "dec" || m == "december" || m == "12":
		day += 3
	case m == "apr" || m == "april" || m == "4":
		m = "April"
		day += 2
	case m == "jun" || m == "june" || m == "6":
		m = "June"
		day += 2
	case m == "sep" || m == "september" || m == "9":
		m = "September"
		day += 2
	case m == "nov" || m == "november" || m == "11":
		m = "November"
		day += 2
	default:
		fmt.Println("Some thing went wrong, please enter the correct month and year")
		return
	}
	fmt.Printf("%q of %v has %v days\n", m, y, day)
}

func leapYear(l int) bool {
	if l%4 == 0 && (l%100 != 0 || l%400 == 0) {
		return true
	}
	return false
}
