package main

import (
	"fmt"
	"gobc3/package/sep"
)

// ---------------------------------------------------------
// EXERCISE: Sum the Numbers: Verbose Edition
//
//  By using a loop, sum the numbers between 1 and 10.
//
// HINT
//  1. For printing it as in the expected output, use Print
//     and Printf functions. They don't print a newline
//     automatically (unlike a Println).
//
//  2. Also, you need to use an if statement to prevent
//     printing the last plus sign.
//
// EXPECTED OUTPUT
//    1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 = 55
// ---------------------------------------------------------

func main() {
	var (
		num int
		i   int
	)

	sep.Cust("Solution 1")
	for i = 1; i < 10; i++ {
		num += i
		fmt.Print(i)
		fmt.Print(" + ")
	}
	fmt.Printf("%d = %d\n", i, num+i)

	sep.Cust("Solution 2")
	solution2()
}

// Solution 2
func solution2() {
	const min, max = 1, 10

	var sum int

	for i := min; i <= max; i++ {
		sum += i

		fmt.Print(i)
		if i < max {
			fmt.Print(" + ")
		}
	}
	fmt.Printf(" = %d\n", sum)
}
