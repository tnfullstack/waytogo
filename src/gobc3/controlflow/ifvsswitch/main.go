// If vs Switch example

package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {

	if len(os.Args) != 2 {
		fmt.Println("Please enter an argument")
		return
	}

	m := strings.ToLower(os.Args[1])

	if m == "dec" || m == "jan" || m == "feb" {
		fmt.Println("Winter")
	} else if m == "mar" || m == "apr" || m == "may" {
		fmt.Println("Spring")
	} else if m == "jun" || m == "jul" || m == "aug" {
		fmt.Println("Summer")
	} else if m == "sep" || m == "oct" || m == "nov" {
		fmt.Println("Fall")
	} else {
		fmt.Println("Please enter correct month.")
	}
}
