/*
	Feet to Meters codin exercises
*/

package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

const usage = `
Feet to Meters
--------------
This program converts feet into meters.

Usage:
$ command feet > enter
`

// Everyone know that the program start here func main()
func main() {

	firstCodeBase()

}

// First code base example
func firstCodeBase() {

	const (
		ftm = 0.3048
		fty = ftm / 0.9144
	)

	if len(os.Args) < 2 {
		fmt.Println(strings.TrimSpace(usage))
		return
	}
	arg := os.Args[1]

	feet, _ := strconv.ParseFloat(arg, 64)

	meters := feet * ftm
	yards := math.Round(feet * fty)

	fmt.Printf("%g feet is %g meters and %g yards\n", feet, meters, yards)
}
