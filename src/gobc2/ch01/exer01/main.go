// Hands-on Exercies #1

// Using the short declaration operator, assign these value to variables with identifiers
// x, y, z = 42, "James Bond", true
// Now print the values stored in those variables a single print statement, multiple print statement

package main

import "fmt"

func main() {

	x := 42
	y := "James Bond"
	z := true

	fmt.Println(x, y, z)

	fmt.Println("-----------------------------------------")

	fmt.Println(x)
	fmt.Println(y)
	fmt.Println(z)

}
