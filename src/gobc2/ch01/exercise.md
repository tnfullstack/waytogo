# Chapter 01

## Exercises

1. What is whitespace?
   Answer: Spaces, Taps, new lines, blank lines are known as whitespace, Go does not care about whitespace.

2. What is a comments? What the two ways of writting a comment?
   Answer: The line that start with // is known as comment, comments are used for code explaination and documentation of a code block. Comments are ignore by Go compiler

3. Our program began with **package main.** What should the files in the fmt package begin with?
   Answer: There are two differnt styles of comments: // Single line comment, and /* text */ multiline comments.

4. We used the Println function defined in the fmt package. If you wanted to use the **Exit** function from the os package, would you need to do?
   Answer: add the import "os" statement to import declaration code block.

5. Modify the program we wrote so that instead of printing Hello, World it prints Hello, my name is followed by your name.
   Answer: [See code](./hello/hello.go) 