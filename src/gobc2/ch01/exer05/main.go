/*
Hands-on exercise #5
Building on the code from the previous example
	1. at the package level scope, using the “var” keyword, create a VARIABLE with the IDENTIFIER “y”. The variable should be of the UNDERLYING TYPE of your custom TYPE “x”
		a. eg:
	2. in func main
		i. 		this should already be done
		ii. 	print out the value of the variable “x”
		iii.	print out the type of the variable “x”
		iv.		assign your own VALUE to the VARIABLE “x” using the “=” OPERATOR
				print out the value of the variable “x”
		b. now do this
			now use CONVERSION to convert the TYPE of the VALUE stored in “x” to the UNDERLYING TYPE
				1. then use the “=” operator to ASSIGN that value to “y”
				2. print out the value stored in “y”
				3. print out the type of “y”
*/

package main

import "fmt"

type Name string

var x Name
var a string
var y string

func main() {

	fmt.Printf("%v\n", x)
	fmt.Printf("%T\n", x)

	x = "Chris Nguyen"
	fmt.Println(x)

	a = string(x)
	fmt.Println(a)

	y = a
	fmt.Println(y)

}
