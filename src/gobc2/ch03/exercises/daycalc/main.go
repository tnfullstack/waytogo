/*
	Write a program to calculate, if today is Sunday what will be the day in 45 days?
*/

package main

import "fmt"

func main() {
	n := 85
	sun := 0
	// mon := 1
	// tue := 2
	// wed := 3
	// thu := 4
	// fri := 5
	// sat := 6

	d := dayCalc(sun, n)

	fmt.Printf("If today is %v, in %d days, the day will be %s\n", "Sunday", n, d)
}

func dayCalc(da, n int) string {

	var today string

	d := (da + n) % 7

	switch d >= 0 {
	case d == 0:
		today = "Sunday"
	case d == 1:
		today = "Monday"
	case d == 2:
		today = "Tuesday"
	case d == 3:
		today = "Wednesday"
	case d == 4:
		today = "Thuday"
	case d == 5:
		today = "Friday"
	case d == 6:
		today = "Saturday"
	default:
		today = "Something went wrong!"
	}
	return today
}
