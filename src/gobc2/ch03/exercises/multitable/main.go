/*
	Write a program that display the following table:
	i		i*2		i*3
   ===		===		===
   2		4		6
   3		6		9
   4		8		12
*/

package main

import "fmt"

func main() {

	fmt.Printf("i x 1\ti x 2\ti x 3\ti x 4\ti x 5\ti x 6\ti x 7\ti x 8\ti x 9\ti x 10\n")
	fmt.Printf("=====\t=====\t=====\t=====\t=====\t=====\t=====\t=====\t======\t======\n")
	for i := 1; i <= 10; i++ {
		fmt.Printf("%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\n",
			i*1, i*2, i*3, i*4, i*5, i*6, i*7, i*8, i*9, i*10)
	}
}
