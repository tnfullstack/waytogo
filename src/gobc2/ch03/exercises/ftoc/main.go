/*
	Convert Fahrenheit to Celsious
*/

package main

import "fmt"

func main() {
	fahrenheit := 212.0

	c := ftoc(fahrenheit)
	fmt.Printf("%.2f fahrenheit = %.2f Celsius\n", fahrenheit, c)
}

func ftoc(f float64) float64 {
	return (f - 32.0) * (5.0 / 9.0)
}
