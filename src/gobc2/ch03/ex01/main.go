package main

import "fmt"

func main() {
	var num1, num2 float64
	fmt.Print("Enter first number: ")
	fmt.Scanf("%f", &num1)
	fmt.Print("Enter second number: ")
	fmt.Scanf("%f", &num2)

	output := num1 * num2

	fmt.Println(output)
}
