/***************************************************************************************************
	Unicode
***************************************************************************************************/

package main

import "fmt"

func main() {
	ascii := 'a'
	unicode := '語'
	newline := '\n'

	fmt.Printf("%d %[1]c %[1]q\n", ascii)
	fmt.Printf("%d %[1]c %[1]q\n", unicode)
	fmt.Printf("%d %[1]c %[1]q\n", newline)

	sep()
	unicode1()

	sep()
	unicode2()

}

// more on unicode
func unicode1() {

	fmt.Println("ㅍ ㅎ ㅂ ㅊ ㅈ")
	fmt.Println("\"OK\"")
	fmt.Println(`\"OK\"`)
	fmt.Println(`"OK"`)
	sep()
	fmt.Println(`
		<!DOCTYPE html>
		<html>
		<body>
			<h1>Heading 1</h1>
		</body>
		</html>`)
}

func unicode2() {
	n := 38
	fmt.Printf("%T\n", n)
	fmt.Printf("%7b %3o %d %x %s \n", n, n, n, n, string(n))

	sep()

	n1 := 40
	fmt.Printf("%T\n", n1)
	fmt.Printf("%7b %3o %d %x %s \n", n1, n1, n1, n1, string(n))

	sep()

	c := 'Ꞡ'
	fmt.Printf("%T\n", c)
	fmt.Printf("%7b %3o %d %x %s \n", c, c, c, c, string(c))

	sep()

	c1 := 'ꝣ'
	fmt.Printf("%T\n", c1)
	fmt.Printf("%7b %3o %d %x %s \n", c1, c1, c1, c1, string(c1))

	c3 := 42851
	fmt.Printf("%T\n", c3)
	fmt.Printf("%7b %3o %d %x %s \n", c3, c3, c3, c3, string(c3))

	sep()

	a := 'A'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(a), []byte(string(a)), a)

	b := 'ɇ'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(b), []byte(string(b)), b)

	d := '本'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(d), []byte(string(d)), d)
}

func sep() {
	fmt.Println("--------------------------------------------------------------")
}
