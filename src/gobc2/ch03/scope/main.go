/*
	Veriables scope
*/

package main

import "fmt"

var g int = 50

func main() {

	fmt.Println(g) // 50 (scope of g is above this line)

	{
		g := 20        // scope of g is line 15 through line 20
		fmt.Println(g) // 20
		f := 5.6       // scope of f is line 15 through line 20
		fmt.Println(f) // 5.6
	}

	// fmt.Println(f)	// error - undefined f

	fmt.Println(g) // 50

	g += 2 // 50 + 2 = 52
	{
		g += 5         // 52 + 5 = 57
		fmt.Println(g) // 57
	}

	sayHello()

}

func sayHello() {
	fmt.Println("Hello!") // Hello!
	fmt.Println(g)        // 57

	s := "from testScore()"
	fmt.Println(s) // from testScore()
}
