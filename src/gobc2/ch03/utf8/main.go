/*
	UTF-8
*/

package main

import "fmt"

func main() {
	sep("UTF-8 Part 1")
	utf8p1()

	sep("UTF-8 Part 2 | くいりりらねてらすりし = hello, world")
	utf8p2()

	sep("UTF-8 Part 3 - 語")
	utf8p3()

	sep("UTF-8 Part 4 | len(str) and Rune Count")
	utf8p4()

	sep("UTF-8 Part 5 | convert str to rune r")
	utf8p5()

	sep("UTF-8 Part 6 | Append string to []rune")
	utf8p6()

}

// utf8 example 1
func utf8p1() {

	n := 38
	fmt.Printf("%T\n", n)
	fmt.Printf("%7b %3o %d %x %s \n", n, n, n, n, string(n))

	sep("")
	n1 := 40
	fmt.Printf("%T\n", n1)
	fmt.Printf("%7b %3o %d %x %s \n", n1, n1, n1, n1, string(n))

	sep("")
	c := 'Ꞡ'
	fmt.Printf("%T\n", c)
	fmt.Printf("%7b %3o %d %x %s \n", c, c, c, c, string(c))

	sep("")
	c1 := 'ꝣ'
	fmt.Printf("%T\n", c1)
	fmt.Printf("%7b %3o %d %x %s \n", c1, c1, c1, c1, string(c1))

	c3 := 42851
	fmt.Printf("%T\n", c3)
	fmt.Printf("%7b %3o %d %x %s \n", c3, c3, c3, c3, string(c3))

	sep("")
	a := 'A'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(a), []byte(string(a)), a)

	b := 'ɇ'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(b), []byte(string(b)), b)

	d := '本'
	fmt.Printf("%v \t %v \t\t\t %b\n", string(d), []byte(string(d)), d)
}

// utf8 example 2
func utf8p2() {

	str := "くいりりらねてらすりし" // hello, world in Japanese

	for i, l := range str {
		fmt.Printf("%#U starts at byte position %d\n", l, i)
	}
	sep("[]byte(str)")
	fmt.Println([]byte(str))
	sep("Number of bytes")
	fmt.Println(len(str))

	sep("ボABCD")
	str = "ボABCD"
	for i, l := range str {
		fmt.Printf("%#U starts at byte position %d\n", l, i)
	}
	sep("[]byte(str)")
	fmt.Println([]byte(str))
	sep("Number of bytes")
	fmt.Println(len(str))
}

// utf8 example 3
func utf8p3() {
	str := "語"
	bytes := []byte(str)

	sep("Decimal; Unicode")
	fmt.Println(bytes)
	fmt.Printf("%U \n", bytes)

	sep("Decimal")
	for _, b := range bytes {
		fmt.Printf("%d ", b)
	}

	sep("Octal")
	for _, b := range bytes {
		fmt.Printf("%o ", b)
	}

	sep("Hex")
	for _, b := range bytes {
		fmt.Printf("%x ", b)
	}

	sep("Convert from hex, and otal back to string")
	fmt.Println("\xe8\xaa\x9e")
	fmt.Println("\350\252\236")
}

// utf8 example 4
func utf8p4() {
	str := "Helloボキ"
	fmt.Println(str)
	fmt.Printf("%v bytes \n", len(str))
}

// utf8 example 5
func utf8p5() {

	str := "学 校 和 製 漢 字"

	fmt.Printf("% x \n", str)

	r := []rune(str)
	fmt.Printf("% x \n", r)

	fmt.Println(string(r))

}

// utf8 Example 6
func utf8p6() {

	var runes []rune

	for _, r := range "bye 製 漢" {
		runes = append(runes, r)
	}
	fmt.Printf("%q\n", runes)

	fmt.Printf("%q\n", []rune("bye 製 漢"))

}

// separator
func sep(s string) string {
	fmt.Printf("\n--------------%v--------------\n", s)
	return s
}
