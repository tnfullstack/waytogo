# Exercises

Q1. How do you access the fourth element of an array or slices?  
Answer: Use name of the array or slice combine with array index number.

    Example array[3:4] will return value from index 3 upto, but not include index 4.

Q2. What is the length of a slice created using make([]int, 3 9)?  
Answer: The length is 3 in a slice's capacity of 9

Q3. Given the following array, what would x[2:5] give you?  
Answer: Value from index 2, 3, and 4

Q4. Write a program that finds the smallest number in this list:  
x := []int{48, 96, 86, 68, 57, 82, 63, 70, 37, 34, 83, 27, 19, 97, 9, 17,}  
Answer: [See the code](src/introgo/ch05/exercises/smlest.go)

