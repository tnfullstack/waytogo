// Creating slice with built-in make function

package main

import "fmt"

func main() {

	// Create a new slice using make
	x := make([]float64, 5) // slice x type = float64, len = 5

	// Add some elements to slice x
	x[0] = 1
	x[1] = 2

	// make allow a third parameter
	y := make([]float64, 5, 10) // slice y's len = 5, capacity = 10
	y = []float64{1, 2, 3, 4, 5}

	fmt.Printf("x : %#v\n", x)
	fmt.Printf("y : %#v\n", y)

	// creating slice using low:high value
	a := []int{1, 2, 3, 4, 5, 6, 7}
	b := a[0:4]
	fmt.Printf("a : %#v\n", a)
	fmt.Printf("b : %#v\n", b)

}
