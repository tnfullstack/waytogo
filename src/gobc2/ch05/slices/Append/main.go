// append adds elements onto the end of a slice. If there is sufficient capacity in the underlying array,
// the element is placed after the last element and the length is incremented.
// However, if there is not sufficient capacity, a new array is created, all of the existing elements are copied
// over, the new element is added onto the end, and the new slice is returned.package main

package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3}
	slice1 = append(slice1, 4, 5, 6)

	fmt.Printf("slice1 : %#v len: %d, cap: %d, %p\n", slice1, len(slice1), cap(slice1), &slice1)

	slice2 := append(slice1, 7, 8, 9)
	fmt.Printf("slice2 : %#v len: %d, cap: %d, %p\n", slice2, len(slice2), cap(slice2), &slice2)

}
