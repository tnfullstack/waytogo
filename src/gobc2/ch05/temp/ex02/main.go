package main

import "fmt"

func main() {
	x := 5
	fmt.Println(zero(x))
	fmt.Println(x)
}

func zero(x int) int {
	x = 0
	return x
}
