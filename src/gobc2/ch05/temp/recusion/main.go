package main

import "fmt"

func main() {
	n := factorial(5)
	fmt.Println(n)
}

func factorial(x uint) uint {
	if x == 0 {
		return 1
	}
	return x * factorial(x-1)
}
