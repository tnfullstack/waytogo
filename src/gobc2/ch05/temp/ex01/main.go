package main

import "fmt"

func main() {

	num := []float64{
		48, 96, 68, 86,
		57, 83, 62, 70,
		37, 34, 83, 27,
		19, 79, 9, 17,
	}

	// Call sum function & print sum result
	s := sum(num)
	fmt.Printf("The sum of %v is %.2f\n", num, s)

	// call average function & print average result
	a := average(num)
	fmt.Printf("The average of %v is %.2f\n", s, a)
}

// Calculate Sum of a list of numbers
func sum(n []float64) float64 {
	sum := 0.0
	for _, v := range n {
		sum += float64(v)
	}
	return sum
}

// Calculate average of a list of numbers
func average(n []float64) float64 {
	sum := 0.0
	for _, v := range n {
		sum += v
	}
	return sum / float64(len(n))
}
