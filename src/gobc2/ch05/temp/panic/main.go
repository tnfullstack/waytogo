// Panic

package main

import "fmt"

func main() {
	defer func() {
		str := recover() // This will never happen
		fmt.Println(str)
	}()
	panic("PANIC")

}

// func recover() {

// }
