package main

import (
	"fmt"
	"os"
	"strconv"
)

const (
	usage = `
	Welcome to odd or even number test.

	To use the command, enter command [integer]
	
	`
)

func main() {

	// Read command-line arguments
	args := os.Args[1:]

	// Validate command-line for errors
	if len(args) != 1 {
		fmt.Println(usage)
		return
	}

	// Convert argument to integer
	num, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Argument is not a number")
		return
	}

	// Validate number is integer
	if num < 0 {
		fmt.Println("Argument is not a number.")
		return
	}

	v, b := isEven(num)

	fmt.Printf("%d / 2 = %v, %t.\n", num, v, b)
}

func isEven(n int) (int, bool) {

	// Check number for even or odd and
	return n / 2, n%2 == 0
}
