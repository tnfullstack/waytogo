package main

import "fmt"

func main() {

	// Call variadic function
	sml := add(42, 2, 34, 11, 5, 33, 7, 8, 9)

	// Print result
	fmt.Println(sml)
}

// Function with one variatic parameter
func add(n ...int) int {

	var greatest = n[0]
	for _, v := range n {
		if v > greatest {
			greatest = v
		} else {
			continue
		}
	}
	return greatest
}
