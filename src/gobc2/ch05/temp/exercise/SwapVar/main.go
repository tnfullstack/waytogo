package main

import "fmt"

func main() {

	// Assign two variables with two integers
	var (
		a = 5
		b = 9
	)
	fmt.Printf("Befor swap a = %v, b = %v\n", a, b)

	// Call swap fuction
	swap(&a, &b)
	fmt.Printf("After swap a = %v, b = %v\n", a, b)

}

func swap(a, b *int) {
	*a, *b = *b, *a
}
