package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {

	// Read command-line argument
	args := os.Args[1:]

	if len(args) != 1 {
		fmt.Println("Please enter a number.")
		return
	}

	num, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Not a number.")
		return
	}

	if num < 0 {
		fmt.Println("Please enter a positive number.")
		return
	}

	// Find fib function
	fmt.Println(findFib(num))
	// Print Result

}

func findFib(n int) int {
	if n == 0 {
		return 1
	}
	return n * findFib(n-1)
}
