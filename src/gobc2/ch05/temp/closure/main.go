// Closure Function

package main

import "fmt"

func main() {

	add := func(a, b int) int {
		return a + b
	}
	fmt.Println(add(2, 5))

	x := 0
	increment := func() int {
		x++
		return x
	}
	fmt.Println(increment())
	fmt.Println(increment())
}
