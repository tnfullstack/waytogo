# Chapter 5 - Exercises

## Array & Slice

1. How do you access the fourth elements of an array or slice? 
    
    Answer: x[3], array or slice name follow with index number inside square bracket.

2. What is the length of a slice created using make([]int, 3, 9)?

    Answer: The length is 3 and capacity is 9

3. Given the following array, what would

    x[2:5] give you?

    x := [6]string{"a","b","c","d","e","f"}

    Answer: [c d e f]

4. Write the program that find the smestest number in this list:
```go
    x := []int{
        48, 96, 86, 68,
        57, 82, 63, 70,
        37, 34, 83, 27,
        19, 97, 9, 17,
    }
```
    Answer: 
    
```go

package main

import "fmt"

func main() {
	x := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 83, 27,
		19, 97, 9, 17,
	}

	min := x[0]
	for i := 1; i < len(x); i++ {
		if x[i] < min {
			min = x[i]
		}
	}
	fmt.Println("The smallest number is:", min)
}

```
