package main

import "fmt"

func main() {
	var x = make(map[string]int)
	x["key"] = 10
	x["car"] = 100
	x["Apple"] = 2
	fmt.Println(x)
	fmt.Println(x["key"], x["car"], x["Apple"])

	delete(x, "car")
	fmt.Println(x)
}
