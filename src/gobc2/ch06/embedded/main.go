package main

import "fmt"

type Person struct {
	FirstName, LastName, Job string
	Age                      int
}

func main() {

	c := Person{
		FirstName: "Chris",
		LastName:  "Nguyen",
		Job:       "Programmer",
		Age:       50,
	}

	fmt.Println(c.FirstName, c.LastName, c.Job, c.Age)

	c.Talk()

}

func (p *Person) Talk() {
	fmt.Printf("Hi, my name is %v %v, I am a %v, I am %v years old!\n",
		p.FirstName, p.LastName, p.Job, p.Age)
}
