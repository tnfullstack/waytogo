package main

import "fmt"

// Shape interface is amazing idea in Go programing

type Shape interface {
	area() float64
}

type MultiShape struct {
	shapes []Shape
}

func main() {

	multiShape := MultiShape{
		shapes: []Shape{
			Circle{0, 0, 5},
			Rectangle{x1: 0, y1: 0, x2: 10, y2: 20},
		},
	}

	fmt.Println(totalArea(&c, &r))
}

// func (c *Circle) area() float64 {
// 	return math.Pi * c.r * c.r
// }

// func (r *Rectangle) area() float64 {
// 	l := distance(r.x1, r.y1, r.x1, r.y2)
// 	w := distance(r.x1, r.y1, r.x2, r.y1)
// 	fmt.Println(l, w)
// 	return l * w
// }

func totalArea(shapes ...Shape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}

func (m *MultiShape) area() float64 {
	var area float64
	for _, s := range m.shapes {
		area += s.area()
	}
	return area
}
