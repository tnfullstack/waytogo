/*
Hands-on exercise #1
Print every number from 1 to 10,000
*/

package main

import "fmt"

func main() {
	var i int
	for i < 10000 {
		i++
		fmt.Println(i)
	}
}
