/* Exercise 1: What does the following program print?

i := 10
if i > 10 {					// i !> 10
	fmt.Println("Big")		// this line will not run
} else {
	fmt.Println("Small")	// this line of code will run to print "Small"
}

*/

