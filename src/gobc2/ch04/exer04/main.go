package main

import "fmt"

func main() {
	for i := 0; i <= 100; i++ {
		fmt.Printf("%v\t%T\t%x\t%U\t%#U\n", i, i, i, i, i)
	}
}
