package main

import "fmt"

func main() {

	switchExam01()

	switchExam02()
}

// switch example 1
func switchExam01() {

	fmt.Print("Enter a number: ")

	var input float64
	fmt.Scanf("%f", &input)

	switch input {
	case 0:
		fmt.Println("Zero")
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	case 3:
		fmt.Println("Three")
	case 4:
		fmt.Println("Four")
	case 5:
		fmt.Println("Five")
	case 6:
		fmt.Println("Six")
	default:
		fmt.Println("Unknown Number")
	}
}

// Switch example 02
func switchExam02() {
	var season string

	for season != "exit" {
		fmt.Print("Enter your faverite season (to stop app enter exit): ")
		fmt.Scanf("%s ", &season)

		switch season {

		case "spring":

		}
	}
}
