/*
Hands-on exercise #2
Print every rune code point of the uppercase alphabet three times. Your output should look like this:
65
	U+0041 'A'
	U+0041 'A'
U+0041 'A'
66
	U+0042 'B'
	U+0042 'B'
	U+0042 'B'
 … through the rest of the alphabet characters
*/

package main

import "fmt"

func main() {
	for i := 0; i <= 100; i++ {
		fmt.Printf("%v\t%T\t%x\t%U\t%#U\n", i, i, i, i, i)
		if i >= 65 && i <= 90 {
			for j := 1; j <= 3; j++ {
				fmt.Printf("%#U\n", i)
			}
		}
	}
}
