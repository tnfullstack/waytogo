package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	t := "Testing the strings Containts functions"
	t1 := "Join this string"

	// Get system environment PATH
	p := os.Getenv("PATH")
	fmt.Println(p)

	// t = strings.ToLower(t)
	// Contains(s, substr string) bool
	fmt.Println(strings.Contains(t, "the"))

	// func Count(s, sep string) int
	fmt.Println(strings.Count(t, "n"))

	// func HasPrefix(s, prefix string) bool
	fmt.Println(strings.HasPrefix(t, "/T"))

	// func HasSuffix(s, suffix string) bool
	fmt.Println(strings.HasSuffix(t, "ts"))

	// find Index(s, sep string) int
	fmt.Println(strings.Index(t, "strin"))

	// func Join(a, []string, sep string) string
	fmt.Println(strings.Join([]string{t, t1}, "-"))

	// func Repeat(s, string, count int) string
	fmt.Println(strings.Repeat("s", 5))

	// func Replace(s, old, new string, n int) string
	fmt.Println(strings.Replace(t, "n", "ABC", 3))

	// func Split(s, sep string) []string
	d := strings.Split(p, ":")
	fmt.Println(d)

	// t = strings.ToLower(t)
	// Contains(s, substr string) bool
	for _, v := range d {
		// convert string to lower case
		v = strings.ToLower(v)
		if strings.Contains(v, "users") {
			println(v)
		}
	}

	// Convert string to slice of bytes anf vice versa
	arr := []byte(t1)
	fmt.Printf(`Conver this string "%v" to slice of bytes`+"\n", t1)
	fmt.Println(arr)

	str := string(arr)
	fmt.Println(str)
}
