package main

import "fmt"

func main() {

	fmt.Println(true && true)  // True
	fmt.Println(true && false) // False
	fmt.Println(true || true)  // True
	fmt.Println(true || false) // True
	fmt.Println(!true)         // False

	fmt.Println()
	exam01()
}

// More example on bool
func exam01() {

	var a = true
	var b = false

	fmt.Println(a || b)
	fmt.Println(a && b)
	fmt.Println(a && !b)
	fmt.Println(true && !!false)
}
