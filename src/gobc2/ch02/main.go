package main

import "fmt"

func main() {

	fmt.Println("1 + 1 = ", 1+1)             // Integer
	fmt.Printf("2.0 + 3.0 = %2.2f\n", 2.0+3) // Floating point

}
