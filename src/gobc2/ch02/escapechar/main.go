/**************************************************************************************************
	Escapes Characters Practice
**************************************************************************************************/

package main

import "fmt"

func main() {
	fmt.Println("Name\t\tAddress\t\t\tPhone Number")
	fmt.Println("----------------------------------------------------------------")
	fmt.Print("James Bond\t007 James Bond St.\t007-007-0070\n")

	fmt.Println()
	escapes01()

}

func escapes01() {
	fmt.Printf("\n%s\t%s\t%s", "Text 1", "Text 2", "Text 3\n")
	fmt.Println("----------------------------------------------------")
	fmt.Printf("\n%s\t%s\t%s", "book 1", "Author", "Publisher\n")
}
