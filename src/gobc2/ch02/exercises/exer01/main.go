/*
Hands-on exercise #1
Write a program that prints a number in decimal, binary, and hex
*/

package main

import "fmt"

func main() {
	a, b, c := 10, 20, 42

	fmt.Printf("%d\t%b\t%x\n", a, a, a)
	fmt.Printf("%d\t%b\t%x\n", b, b, b)
	fmt.Printf("%d\t%b\t%x\n", c, c, c)
}
