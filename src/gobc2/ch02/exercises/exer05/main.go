/***************************************************************************************************
	Hands-on exercise #5
	Create a variable of type string using a raw string literal. Print it.
***************************************************************************************************/
package main

import "fmt"

func main() {
	mes := `This string is inside the backticks,
			also called, string template literal`

	fmt.Println(mes)

}
