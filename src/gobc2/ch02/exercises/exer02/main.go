/*
Hands-on exercise #2
Using the following operators, write expressions and assign their values to variables:
==
<=
>=
!=
<
>
Now print each of the variables.
*/

package main

import "fmt"

func main() {
	var x, y, z int
	var a, b, c string
	var n float64

	x = 15
	z = 20
	y = x
	a, b, c = "Testing", "Go", "Lang"
	n = 32.9

	test1 := y == x
	test2 := z < x
	test3 := y < z

	test4 := x >= z
	test5 := y <= x
	test6 := x != z

	testn := n < float64(x)

	fmt.Println("x =", x, "| y =", y, "| z =", z, "| n =", n)

	fmt.Println("y == x", test1, "| z < x", test2, "| y < z", test3, "| x >= z", test4, "| y <= x", test5, "| x != z", test6, "| n < x", testn)

	fmt.Println("------------------------------------------------------------------------------------")
	test7 := a == b
	test8 := b <= a
	test9 := c >= b
	test10 := a != c
	test11 := a < b
	test12 := a > c

	fmt.Println("a =", a, "| b =", b, "| c =", c)
	fmt.Println("a == b", test7, "| b <= a", test8, "| c >= b", test9, "| a != c", test10, "| a < b", test11, "| a > c", test12)

}
