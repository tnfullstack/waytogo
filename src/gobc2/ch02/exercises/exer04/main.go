/*
Hands-on exercise #4
Write a program that
	* assigns an int to a variable
	* prints that int in decimal, binary, and hex
	* shifts the bits of that int over 1 position to the left, and assigns that to a variable
	* prints that variable in decimal, binary, and hex
*/

package main

import "fmt"

const (
	_ = iota
	// kb = 1024
	kb = 1 << (iota * 10)
	mb = 1 << (iota * 10)
	gb = 1 << (iota * 10)
)

func main() {

	num := 25

	fmt.Printf("%d\t%b\t%x\n", num, num, num)

	num1 := num << 1

	fmt.Printf("%d\t%b\t%x\n", num1, num1, num1)

	fmt.Println("---------------------------------------------------------------------------")

	fmt.Printf("%d\t\t\t%b\t\t\t\t%x\n", kb, kb, kb)
	fmt.Printf("%d\t\t\t%b\t\t\t%x\n", mb, mb, mb)
	fmt.Printf("%d\t\t%b\t\t%x\n", gb, gb, gb)
}
