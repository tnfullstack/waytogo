/*
Hands-on exercise #3
Create TYPED and UNTYPED constants. Print the values of the constants.
*/

package main

import "fmt"

const (
	myName        = "James Bond"
	dob, mob, yob = 07, 07, 7007
)

func main() {

	fmt.Println("Hello! my name is", myName)
	fmt.Println("My bidth day is", mob, "/", dob, "/", yob)
}
