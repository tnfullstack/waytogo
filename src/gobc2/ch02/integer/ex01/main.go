// Integer

package main

import "fmt"

func main() {
	// Integer
	fmt.Println("1 + 1 = ", 1+1)

	// Floating point
	fmt.Printf("2.5 + 3.0 = %.2f\n", 2.5+3)
}
